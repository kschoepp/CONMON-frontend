package client;

import client.assets.charts.BarChart;
import client.assets.charts.GaugeChart;
import client.assets.charts.LineChart;
import client.assets.dialogs.*;
import client.assets.AssetConstants;
import client.config.ConfigService;
import client.config.ConfigServiceAsync;
import client.config.NotifyConfig;
import client.locales.ErrorsEN;
import client.locales.MessagesEN;
import client.remote.SensorService;
import client.remote.SensorServiceAsync;
import client.ui.Menubar;
import client.ui.WidgetContainer;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.googlecode.gwt.charts.client.gauge.Gauge;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyPlacement;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyType;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;
import org.gwtbootstrap3.extras.notify.client.ui.NotifySettings;
import org.jdom2.Element;

import java.util.*;


public class Dashboard implements EntryPoint {

    //Global variables
    private Map<String,String> widgetContainer;
    private int interval;
    //Local UI elements
    private Menubar menubar;
//    private FlowPanel contentFlow;
    //Local assets
    private AddBox addBox;
    private AddRowBox addRowBox;
    private AddWidgetBox addWidgetBox;
    private AddGaugeBox addGaugeBox;
    private AddLineBox addLineBox;
    private AddBarBox addBarBox;
    private ControlPanel controlPanel;
    private SensorBox sensorBox;
    private AddSensorBox addSensorBox;
    private MachineBox machineBox;
    private AddMachineBox addMachineBox;
    private RuleBox ruleBox;
    private AggregateBox aggregateBox;
    private AddEventBox addEventBox;
    private SettingsBox settingsBox;
    //CONMON servlets
    private ConfigServiceAsync configService;
    private SensorServiceAsync sensorService;
    //Notify settings
    private NotifySettings notifyError;
    private NotifySettings notifySuccess;

    //Function loaded on entry point
    public void onModuleLoad() {
        configService = ConfigService.Instance.get();
        sensorService = SensorService.Instance.get();
        interval = 1000;
        loadNotifySettings();
        loadMenubar();
        loadContentFlow();
        loadAssets();
        loadHandlers();
        loadRows();
    }

    private void loadNotifySettings() {
        notifyError = NotifySettings.newSettings();
        notifyError.setType(NotifyType.DANGER);
        notifyError.setPlacement(NotifyPlacement.TOP_CENTER);
        notifySuccess = NotifySettings.newSettings();
        notifySuccess.setType(NotifyType.SUCCESS);
        notifySuccess.setPlacement(NotifyPlacement.TOP_RIGHT);
    }

    private void loadMenubar() {
        menubar = new Menubar();
        RootPanel.get("menubar").add(menubar);
    }

    private void loadContentFlow() {
        widgetContainer = new HashMap<>();
//        contentFlow = new FlowPanel();
//        contentFlow.getElement().setId("contentflow");
//        contentFlow.getElement().getStyle().setDisplay(Style.Display.INLINE);
//        RootPanel.get("content").add(contentFlow);
    }

    private void loadAssets() {
        //AddBox
        addBox = new AddBox();
        RootPanel.get("content").add(addBox);
        addBox.center();
        addBox.hide();
        addBox.setCloseButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addBox.hide();
            }
        });
        addBox.setAddRowHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addRowBox.reload();
                addRowBox.show();
            }
        });
        addBox.setAddWidgetHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addWidgetBox.reload();
                addWidgetBox.show();
            }
        });
        //AddRowBox
        addRowBox = new AddRowBox();
        RootPanel.get("content").add(addRowBox);
        addRowBox.center();
        addRowBox.hide();
        addRowBox.setAddButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                String containerName = addRowBox.getRowName();
                if(containerName.isEmpty() || containerName.length() == 0) {
                    Notify.notify(ErrorsEN.ERROR_CONTAINER_NAME_EMPTY,NotifyConfig.notifyError());
                } else {
                    WidgetContainer newContainer = new WidgetContainer(containerName);
                    widgetContainer.put(newContainer.getName(),newContainer.getId());
                    RootPanel.get("content").add(newContainer);
                    configService.saveRow(newContainer.getId(), newContainer.getName(), new AsyncCallback<Integer>() {
                        @Override
                        public void onFailure(Throwable caught) { }

                        @Override
                        public void onSuccess(Integer result) { }
                    });
                }
            }
        });
        addRowBox.setCancelButton(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addRowBox.hide();
            }
        });
        //AddWidgetBox
        addWidgetBox = new AddWidgetBox();
        RootPanel.get("content").add(addWidgetBox);
        addWidgetBox.center();
        addWidgetBox.hide();
        addWidgetBox.setAddButtonHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                int selected = addWidgetBox.getSelectChart();
                switch (selected) {
                    case AssetConstants.SELECT_GAUGECHART:
                        addGaugeBox.show();
                        addGaugeBox.loadValues(widgetContainer);
                        break;
                    case AssetConstants.SELECT_LINECHART:
                        addLineBox.show();
                        addLineBox.loadValues(widgetContainer);
                        break;
                    case AssetConstants.SELECT_BARCHART:
                        addBarBox.show();
                        addBarBox.loadValues(widgetContainer);
                        break;
                    default:
                        Notify.notify(ErrorsEN.ERROR_NO_CHART_SELECTED, notifyError);
                        break;
                }
            }
        });
        addWidgetBox.setCancelButton(new ClickHandler() {
            public void onClick(ClickEvent event) {
                addWidgetBox.hide();
            }
        });
        //AddGaugeBox
        addGaugeBox = new AddGaugeBox();
        RootPanel.get("content").add(addGaugeBox);
        addGaugeBox.center();
        addGaugeBox.hide();
        addGaugeBox.setCreateButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                configService.getConfig("interval", new AsyncCallback<String>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        Notify.notify(ErrorsEN.ERROR_CHART_CREATION_FAILED, notifyError);
                    }

                    @Override
                    public void onSuccess(String result) {
                        int interval = Integer.parseInt(result);
                        int sid = addGaugeBox.getSelectedSensorID();
                        if(sid == -1) {
                            Notify.notify(ErrorsEN.ERROR_NO_SENSOR_SELECTED, notifyError);
                        } else if(sid == -3) {
                            Notify.notify(ErrorsEN.ERROR_NO_ACTIVE_SENSORS, notifyError);
                        } else {
                            String curRowName = addGaugeBox.getSelectedRow();
                            if(curRowName.equals("err-3")) {
                                Notify.notify(ErrorsEN.ERROR_NO_CONTAINER_SELECTED,NotifyConfig.notifyError());
                            } else {
                                String rowID = widgetContainer.get(curRowName);
                                double gaugeMin = addGaugeBox.getMin();
                                double gaugeMax = addGaugeBox.getMax();
                                GaugeChart gaugeChart = new GaugeChart(rowID,sid,gaugeMin,gaugeMax,interval);
                                gaugeChart.draw();
                                String curCID = gaugeChart.getChartID();
                                configService.saveWidget("gaugeChart", rowID, curCID, Integer.toString(sid), gaugeMin, gaugeMax, new AsyncCallback<Integer>() {
                                    @Override
                                    public void onFailure(Throwable caught) {}

                                    @Override
                                    public void onSuccess(Integer result) {}
                                });
                            }
                        }
                    }
                });
            }
        });
        addGaugeBox.setCancelButtonHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                addGaugeBox.hide();
            }
        });
        //AddLineBox
        addLineBox = new AddLineBox();
        RootPanel.get("content").add(addLineBox);
        addLineBox.center();
        addLineBox.hide();
        addLineBox.setCancelButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addLineBox.hide();
            }
        });
        addLineBox.setCreateButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                configService.getConfig("interval", new AsyncCallback<String>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        Notify.notify(ErrorsEN.ERROR_CHART_CREATION_FAILED, notifyError);
                    }

                    @Override
                    public void onSuccess(String result) {
                        int interval = Integer.parseInt(result);
                        int sid = addLineBox.getSelectedSensorID();
                        if (sid == -1) {
                            Notify.notify(ErrorsEN.ERROR_NO_SENSOR_SELECTED, notifyError);
                        } else if (sid == -3) {
                            Notify.notify(ErrorsEN.ERROR_NO_ACTIVE_SENSORS, notifyError);
                        } else {
                            String curRowName = addLineBox.getSelectedRow();
                            if (curRowName.equals("err-3")) {
                                Notify.notify(ErrorsEN.ERROR_NO_CONTAINER_SELECTED, NotifyConfig.notifyError());
                            } else {
                                String rowID = widgetContainer.get(curRowName);
                                LineChart lineChart = new LineChart(rowID,sid,interval);
                                lineChart.draw();
                            }
                        }
                    }
                });
            }
        });
        //AddBarBox
        addBarBox = new AddBarBox();
        RootPanel.get("content").add(addBarBox);
        addBarBox.center();
        addBarBox.hide();
        addBarBox.setCancelButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addBarBox.hide();
            }
        });
        addBarBox.setCreateButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                configService.getConfig("interval", new AsyncCallback<String>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        Notify.notify(ErrorsEN.ERROR_CHART_CREATION_FAILED, notifyError);
                    }

                    @Override
                    public void onSuccess(String result) {
                        int interval = Integer.parseInt(result);
                        int sid = addBarBox.getSelectedSensorID();
                        if (sid == -1) {
                            Notify.notify(ErrorsEN.ERROR_NO_SENSOR_SELECTED, notifyError);
                        } else if (sid == -3) {
                            Notify.notify(ErrorsEN.ERROR_NO_ACTIVE_SENSORS, notifyError);
                        } else {
                            String curRowName = addBarBox.getSelectedRow();
                            if (curRowName.equals("err-3")) {
                                Notify.notify(ErrorsEN.ERROR_NO_CONTAINER_SELECTED, NotifyConfig.notifyError());
                            } else {
                                String rowID = widgetContainer.get(curRowName);
                                BarChart barChart = new BarChart(rowID,sid,interval);
                                barChart.draw();
                            }
                        }
                    }
                });
            }
        });
        //Infobox
        controlPanel = new ControlPanel();
        RootPanel.get("content").add(controlPanel);
        controlPanel.center();
        controlPanel.hide();
        controlPanel.setCloseButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                controlPanel.hide();
            }
        });
        controlPanel.setMachineButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                machineBox.show();
                machineBox.loadValues();
            }
        });
        controlPanel.setSensorButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                sensorBox.show();
                sensorBox.loadValues();
            }
        });
        controlPanel.setRuleButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ruleBox.show();
                ruleBox.loadValues();
            }
        });
        controlPanel.setAggregateButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                aggregateBox.show();
                aggregateBox.loadValues();
            }
        });
        controlPanel.setAddEventButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addEventBox.center();
                addEventBox.show();
            }
        });
        addEventBox = new AddEventBox();
        RootPanel.get("content").add(addEventBox);
        addEventBox.hide();
        //Machinebox
        machineBox = new MachineBox();
        RootPanel.get("content").add(machineBox);
        machineBox.center();
        machineBox.hide();
        machineBox.setAddButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addMachineBox.show();
            }
        });
        machineBox.setCloseButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                machineBox.hide();
            }
        });
        //AddSensorBox
        addMachineBox = new AddMachineBox();
        RootPanel.get("content").add(addMachineBox);
        addMachineBox.center();
        addMachineBox.hide();
        addMachineBox.setAddButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                Map<String,String> newMachine = addMachineBox.getMachineData();
                if(newMachine == null) {
                    Notify.notify(ErrorsEN.ERROR_MACHINENAME_REQUIRED,NotifyConfig.notifyError());
                } else {
                    String machineName = newMachine.get("machineName");
                    String uri = newMachine.get("uri");
                    String location = newMachine.get("location");
                    String description = newMachine.get("description");
                    sensorService.addMachine(machineName, uri, location, description, new AsyncCallback<Integer>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            //TODO
                        }

                        @Override
                        public void onSuccess(Integer result) {
                            if(result == 1) {
                                Notify.notify(MessagesEN.MESSAGE_MACHINE_ADDED,NotifyConfig.notifySuccess());
                            } else {
                                Notify.notify(ErrorsEN.ERROR_MACHINE_NOT_ADDED,NotifyConfig.notifyError());
                            }
                        }
                    });
                }
            }
        });
        addMachineBox.setCancelButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addMachineBox.hide();
            }
        });
        //Sensorbox
        sensorBox = new SensorBox();
        RootPanel.get("content").add(sensorBox);
        sensorBox.center();
        sensorBox.hide();
        sensorBox.setAddButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addSensorBox.show();
                addSensorBox.loadValues();
            }
        });
        sensorBox.setCloseButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                sensorBox.hide();
            }
        });
        //AddSensorBox
        addSensorBox = new AddSensorBox();
        RootPanel.get("content").add(addSensorBox);
        addSensorBox.center();
        addSensorBox.hide();
        addSensorBox.setAddButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                Map<String,String> newSensor = addSensorBox.getSensorData();
                if(newSensor != null) {
                    sensorService.addSensor(newSensor.get("sensorName"), newSensor.get("machineName"), newSensor.get("machineID"), newSensor.get("namespace"), newSensor.get("nodeid"), newSensor.get("valueType"), newSensor.get("delta"), newSensor.get("unit"), new AsyncCallback<Integer>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            //TODO
                        }

                        @Override
                        public void onSuccess(Integer result) {
                            switch (result) {
                                case 1:
                                    Notify.notify(MessagesEN.MESSAGE_SENSOR_ADDED, NotifyConfig.notifySuccess());
                                    break;
                                default:
                                    Notify.notify(ErrorsEN.ERROR_SENSOR_NOT_ADDED, NotifyConfig.notifyError());
                                    break;
                            }
                        }
                    });
                }
            }
        });
        addSensorBox.setCancelButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addSensorBox.hide();
            }
        });
        //RuleBox
        ruleBox = new RuleBox();
        RootPanel.get("content").add(ruleBox);
        ruleBox.center();
        ruleBox.hide();
        ruleBox.setAddButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                AddRuleBox newRule = new AddRuleBox();
                RootPanel.get("content").add(newRule);
                newRule.center();
                newRule.show();
            }
        });
        ruleBox.setCloseButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ruleBox.hide();
            }
        });
        //AggregateBox
        aggregateBox = new AggregateBox();
        RootPanel.get("content").add(aggregateBox);
        aggregateBox.center();
        aggregateBox.hide();
        aggregateBox.setAddButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                AddAggregateBox newAggregate = new AddAggregateBox();
                RootPanel.get("content").add(newAggregate);
                newAggregate.center();
                newAggregate.show();
            }
        });
        aggregateBox.setCloseButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                aggregateBox.hide();
            }
        });
        //Settingsbox
        settingsBox = new SettingsBox();
        RootPanel.get("content").add(settingsBox);
        settingsBox.center();
        settingsBox.hide();
        settingsBox.setSaveButtonHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                settingsBox.setSaveButtonLoading();
                String notificationsEnabled;
                if(settingsBox.getNotificationsSettings()) {
                    notificationsEnabled = "enabled";
                } else {
                    notificationsEnabled = "disabled";
                }
                configService.writeConfigs(settingsBox.getServername(), settingsBox.getServerport(), settingsBox.getInterval(), notificationsEnabled, new AsyncCallback<Integer>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        settingsBox.setSaveButtonNotLoading();
                        //TODO
                    }

                    @Override
                    public void onSuccess(Integer result) {
                        settingsBox.setSaveButtonNotLoading();
                        if(result == 1) {
                            Notify.notify(MessagesEN.MESSAGE_SETTINGS_SAVED,notifySuccess);
                            settingsBox.hide();
                        } else {
                            Notify.notify(ErrorsEN.ERROR_SETTINGS_NOT_SAVED,notifyError);
                        }
                    }
                });
            }
        });
        settingsBox.setCancelButtonHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                settingsBox.hide();
            }
        });
    }

    private void loadHandlers() {
        //Menubar add widget button
        menubar.setWidgetHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                addBox.show();
            }
        });
        //Menubar info button
        menubar.setInfoHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                controlPanel.show();
            }
        });
        //Menubar settings button
        menubar.setSettingsHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                settingsBox.loadValues();
                settingsBox.show();
            }
        });
    }

    private void loadRows() {
        configService.getRows(new AsyncCallback<Map<String, String>>() {
            @Override
            public void onFailure(Throwable caught) { }

            @Override
            public void onSuccess(Map<String, String> result) {
                if(!result.isEmpty()) {
                    Set<String> rowKeys = result.keySet();
                    Iterator<String> rowIt = rowKeys.iterator();
                    while (rowIt.hasNext()) {
                        String curKey = rowIt.next();
                        WidgetContainer newContainer = new WidgetContainer(result.get(curKey),curKey);
                        widgetContainer.put(newContainer.getName(),newContainer.getId());
                        RootPanel.get("content").add(newContainer);
                    }
                    loadInterval();
                }
            }
        });
    }

    private void loadInterval() {
        configService.getConfig("interval", new AsyncCallback<String>() {
            @Override
            public void onFailure(Throwable caught) { }

            @Override
            public void onSuccess(String result) {
                interval = Integer.parseInt(result);
                loadWidgets();
            }
        });
    }

    private void loadWidgets() {
        configService.getWidgets(new AsyncCallback<ArrayList<Map<String, String>>>() {
            @Override
            public void onFailure(Throwable caught) { }

            @Override
            public void onSuccess(ArrayList<Map<String, String>> result) {
                if(!result.isEmpty()) {
                    for (int i=0;i<result.size();i++) {
                        Map<String,String> curWidget = result.get(i);
                        if (curWidget.get("type").equals("gaugeChart")) {
                            GaugeChart newGauge = new GaugeChart(curWidget.get("rowID"),Integer.parseInt(curWidget.get("sensorID")),Double.parseDouble(curWidget.get("min")),Double.parseDouble(curWidget.get("max")),interval);
                            newGauge.draw();
                        }
                    }
                }
            }
        });
    }

}
