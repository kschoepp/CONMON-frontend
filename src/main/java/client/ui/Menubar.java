package client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;


public class Menubar extends Composite {
    interface MenubarUiBinder extends UiBinder<HTMLPanel, Menubar> {
    }

    private static MenubarUiBinder ourUiBinder = GWT.create(MenubarUiBinder.class);

    @UiField
    Anchor navAddWidget;
    @UiField
    Anchor navOpenInfo;
    @UiField
    Anchor navOpenSettings;

    public Menubar() {
        initWidget(ourUiBinder.createAndBindUi(this));
    }

    public void setWidgetHandler(ClickHandler clickHandler) {
        navAddWidget.addClickHandler(clickHandler);
    }

    public void setInfoHandler(ClickHandler clickHandler) {
        navOpenInfo.addClickHandler(clickHandler);
    }

    public void setSettingsHandler(ClickHandler clickHandler) {
        navOpenSettings.addClickHandler(clickHandler);
    }

}