package client.ui;

import client.config.ContainerConfig;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.FlowPanel;
import org.gwtbootstrap3.client.ui.Heading;
import org.gwtbootstrap3.client.ui.constants.HeadingSize;

public class WidgetContainer extends FlowPanel {

    //Global variables
    private Heading heading;
    private String containerID;

    public WidgetContainer(String heading) {
        this.heading = new Heading(HeadingSize.H3);
        this.heading.setText(heading);
        this.containerID = ContainerConfig.getNextContainerID(); //heading.replaceAll("\\s","");
        loadStyle();
        loadHeading();
    }

    public WidgetContainer(String heading, String containerID) {
        this.heading = new Heading(HeadingSize.H3);
        this.heading.setText(heading);
        this.containerID = containerID;
        loadStyle();
        loadHeading();
    }

    private void loadStyle() {
        heading.setMarginLeft(20);
        heading.getElement().getStyle().setFontWeight(Style.FontWeight.BOLD);
        this.getElement().getStyle().setDisplay(Style.Display.INLINE);
        this.getElement().setId(containerID);
    }

    private void loadHeading() {
        this.add(heading);
    }

    public String getId() {
        return containerID;
    }

    public String getName() {
        return heading.getText();
    }

}
