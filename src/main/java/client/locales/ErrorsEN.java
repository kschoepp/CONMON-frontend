package client.locales;


public class ErrorsEN {

    //Container erros
    public static final String ERROR_CONTAINER_NAME_EMPTY = "Please insert a name for the new row!";
    public static final String ERROR_NO_CONTAINER_SELECTED = "Please select a row to add the widget!";
    //Dialog errors
    public static final String ERROR_NO_CHART_SELECTED = "Please select a chart!";
    public static final String ERROR_NO_SENSOR_SELECTED = "Please select a sensor!";
    public static final String ERROR_NO_ACTIVE_SENSORS = "Can't create a chart because there are no active sensors!";
    public static final String ERROR_FIELD_REQUIRED_1 = "Field <";
    public static final String ERROR_FIELD_REQUIRED_2 = "> is required!";
    public static final String ERROR_MACHINENAME_REQUIRED = "Field <Machinename> is required!";
    //RPC errors
    public static final String ERROR_RPC = "Unable to receive any data from RPC Server!";
    //Chart errors
    public static final String ERROR_CHART_CREATION_FAILED = "Can't create a chart because the chart configs are not readable";
    public static final String ERROR_CHART_CREATION_READ_SENSOR = "Error while creating chart. Can't receive any sensor informations";
    public static final String ERROR_CHART_CREATION_NO_DATA = "Unable to create a new chart by the selected sensor because there are no sensor values received from OPC server yet!";
    //Settings errors
    public static final String ERROR_SETTINGS_NOT_SAVED = "Unable to save settings properties!";
    //Sensor errors
    public static final String ERROR_SENSOR_NOT_REMOVED = "Unable to remove the selected sensor!";
    public static final String ERROR_SENSOR_NOT_ADDED = "Error while adding new sensor to the server!";
    //Machine errors
    public static final String ERROR_MACHINE_NOT_ADDED = "Error while adding new machine to the server!";
    public static final String ERROR_MACHINE_NOT_REMOVED = "Unable to remove the selected machine!";
    public static final String ERROR_MACHINE_NOT_REMOVED_ACTIVE_SENSORS = "Unable to remove the selected machine because there are already some active sensors!";
    //Rule errors
    public static final String ERROR_RULE_NOT_ADDED = "Error while adding rule to database!";
    public static final String ERROR_RULE_NOT_REMOVED = "Error while removing rule from database!";
    //Aggregate errors
    public static final String ERROR_AGGREGATE_NOT_ADDED = "Error while adding aggregate to database";
    public static final String ERROR_AGGREGATE_NOT_REMOVED = "Error while removing aggregate from database!";


}
