package client.locales;

public class MessagesEN {

    //Settings messages
    public static final String MESSAGE_SETTINGS_SAVED = "Settings successfully saved!";
    //Sensor messages
    public static final String MESSAGE_SENSOR_REMOVED = "Sensor successfully removed!";
    public static final String MESSAGE_SENSOR_ADDED = "Sensor successfully added!";
    //Machine messages
    public static final String MESSAGE_MACHINE_ADDED = "Machine successfully added!";
    public static final String MESSAGE_MACHINE_REMOVED = "Machine successfully removed!";
    //Rule messages
    public static final String MESSAGE_RULE_ADDED = "Rule successfully added!";
    public static final String MESSAGE_RULE_REMOVED = "Rule successfully removed!";
    //Aggregate messages
    public static final String MESSAGE_AGGREGATE_ADDED = "Aggregate successfully added!";
    public static final String MESSAGE_AGGREGATE_REMOVED = "Aggregate successfully removed!";

}
