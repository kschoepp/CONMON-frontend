package client.config;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;

import java.util.ArrayList;
import java.util.Map;


@RemoteServiceRelativePath("ConfigService")
public interface ConfigService extends RemoteService {
    /**
     * Utility/Convenience class.
     * Use ConfigService.App.getInstance() to access static instance of ConfigServiceAsync
     */
    class Instance {
        private static final ConfigServiceAsync ourInstance = (ConfigServiceAsync) GWT.create(ConfigService.class);

        public static ConfigServiceAsync get() {
            return ourInstance;
        }
    }

    String getConfig(String configParam);

    int writeConfigs(String serverName, String serverPort, String interval, String notificationsEnabled);

    int saveWidget(String widgetType, String rowID, String chartID, String sid, double min, double max);

    int saveRow(String rowID, String rowName);

    Map<String,String> getRows();

    ArrayList<Map<String,String>> getWidgets();

}
