package client.config;

import org.gwtbootstrap3.extras.notify.client.constants.NotifyPlacement;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyType;
import org.gwtbootstrap3.extras.notify.client.ui.NotifySettings;


public class NotifyConfig {

    private static NotifySettings notifyError = NotifySettings.newSettings();
    private static NotifySettings notifySuccess = NotifySettings.newSettings();

    public static NotifySettings notifyError() {
        notifyError.setType(NotifyType.DANGER);
        notifyError.setPlacement(NotifyPlacement.TOP_CENTER);
        return notifyError;
    }

    public static NotifySettings notifySuccess() {
        notifySuccess.setType(NotifyType.SUCCESS);
        notifySuccess.setPlacement(NotifyPlacement.TOP_RIGHT);
        return notifySuccess;
    }

}
