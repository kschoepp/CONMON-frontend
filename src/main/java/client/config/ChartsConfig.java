package client.config;


public class ChartsConfig {

    private static int chartCounter = 0;

    public static String getChartId() {
        chartCounter++;
        String id = "chart"+chartCounter;
        return id;
    }

    public static void pushCounter() {
        chartCounter++;
    }

}
