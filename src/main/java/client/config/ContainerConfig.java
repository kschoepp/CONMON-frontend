package client.config;

public class ContainerConfig {

    private static int containerID = 0;

    public static String getNextContainerID() {
        containerID++;
        return "container"+containerID;
    }

}
