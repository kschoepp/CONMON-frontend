package client.config;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.ArrayList;
import java.util.Map;



public interface ConfigServiceAsync {

    void getConfig(String configParam, AsyncCallback<String> callback);

    void writeConfigs(String serverName, String serverPort, String interval, String notificationsEnabled, AsyncCallback<Integer> callback);

    void saveWidget(String widgetType, String rowID, String chartID, String sid, double min, double max, AsyncCallback<Integer> callback);

    void saveRow(String rowID, String rowName, AsyncCallback<Integer> callback);

    void getRows(AsyncCallback<Map<String,String>> callback);

    void getWidgets(AsyncCallback<ArrayList<Map<String,String>>> callback);

}
