package client.assets.charts;


import client.config.ChartsConfig;
import client.config.NotifyConfig;
import client.locales.ErrorsEN;
import client.remote.SensorService;
import client.remote.SensorServiceAsync;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyPlacement;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyType;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;
import org.gwtbootstrap3.extras.notify.client.ui.NotifySettings;

import java.util.Map;


public class GaugeChart {

    //Global variables
    private String containerID;
    private String chartID;
    private String sensorName;
    private int sid;
    private double min;
    private double max;
    private int interval;
    private String unit;
    private double value;
    //Servlets
    private SensorServiceAsync sensorService;

    public GaugeChart(String containerID, int sid, double min, double max, int interval) {
        chartID = ChartsConfig.getChartId();
        this.containerID = containerID;
        this.sid = sid;
        this.min = min;
        this.max = max;
        this.interval = interval;
        this.sensorService = SensorService.Instance.get();
    }

    public void draw() {
        sensorService.getSensorInfo(sid, new AsyncCallback<Map<String, String>>() {
            @Override
            public void onFailure(Throwable caught) {
                NotifySettings notifySettings = NotifySettings.newSettings();
                notifySettings.setPlacement(NotifyPlacement.TOP_CENTER);
                notifySettings.setType(NotifyType.DANGER);
                Notify.notify(ErrorsEN.ERROR_CHART_CREATION_READ_SENSOR, notifySettings);
            }

            @Override
            public void onSuccess(Map<String, String> result) {
                sensorName = result.get("sensorName");
                unit = result.get("unit");
                if(!result.get("value").isEmpty()) {
                    value = Double.parseDouble(result.get("value"));
                    createChart(containerID, chartID, sensorName, min, max, value, interval, sid, unit);
                    startUpdater();
                } else {
                    Notify.notify(ErrorsEN.ERROR_CHART_CREATION_NO_DATA, NotifyConfig.notifyError());
                }
            }
        });
    }

    public String getChartID() {
        return chartID;
    }

    private void startUpdater() {
        Timer timer = new Timer() {
            @Override
            public void run() {
                updateSensorValue();
            }
        };
        timer.scheduleRepeating(interval);
    }

    private void updateSensorValue() {
        sensorService.getSensorInfo(sid, new AsyncCallback<Map<String, String>>() {
            @Override
            public void onFailure(Throwable caught) {
                //TODO
            }

            @Override
            public void onSuccess(Map<String, String> result) {
                value = Double.parseDouble(result.get("value"));
            }
        });
    }

    public double getValue() {
        return value;
    }

    private native void createChart(String containerID, String chartID, String sensorName, double min, double max, double value, int interval, int sid, String unit) /*-{
        var that = this;
        var contentflow = $doc.getElementById(containerID);
        var gaugeDiv = $doc.createElement("span");
        gaugeDiv.id = chartID;
        gaugeDiv.style = "width: 300px; height: 300px; float: left";
        contentflow.appendChild(gaugeDiv);
        var gauge = new $wnd.JustGage({
           id: chartID,
           min: min,
           max: max,
           title: sensorName,
           value: value,
           label: unit
        });

        setInterval(function () {
            gauge.refresh(that.@client.assets.charts.GaugeChart::getValue()());
        }, interval);
    }-*/;

}
