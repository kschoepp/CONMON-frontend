package client.assets.charts;

import client.config.ChartsConfig;
import client.config.NotifyConfig;
import client.locales.ErrorsEN;
import client.remote.SensorService;
import client.remote.SensorServiceAsync;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyPlacement;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyType;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;
import org.gwtbootstrap3.extras.notify.client.ui.NotifySettings;

import java.util.Map;

public class BarChart {

    //Global variables
    private String containerID;
    private String chartID;
    private String sensorName;
    private int sid;
    private int interval;
    private double value;
    //Servlets
    private SensorServiceAsync sensorService;

    public BarChart(String containerID, int sid, int interval) {
        chartID = ChartsConfig.getChartId();
        this.containerID = containerID;
        this.sid = sid;
        this.interval = interval;
        this.sensorService = SensorService.Instance.get();
    }

    public void draw() {
        sensorService.getSensorInfo(sid, new AsyncCallback<Map<String, String>>() {
            @Override
            public void onFailure(Throwable caught) {
                NotifySettings notifySettings = NotifySettings.newSettings();
                notifySettings.setPlacement(NotifyPlacement.TOP_CENTER);
                notifySettings.setType(NotifyType.DANGER);
                Notify.notify(ErrorsEN.ERROR_CHART_CREATION_READ_SENSOR, notifySettings);
            }

            @Override
            public void onSuccess(Map<String, String> result) {
                if(!result.get("value").isEmpty()) {
                    sensorName = result.get("sensorName");
                    value = Double.parseDouble(result.get("value"));
                    createChart(containerID,chartID,sensorName,value,interval,sid);
                    startUpdater();
                } else {
                    Notify.notify(ErrorsEN.ERROR_CHART_CREATION_NO_DATA, NotifyConfig.notifyError());
                }
            }
        });
    }

    private void startUpdater() {
        Timer timer = new Timer() {
            @Override
            public void run() {
                updateSensorValue();
            }
        };
        timer.scheduleRepeating(interval);
    }

    private void updateSensorValue() {
        sensorService.getSensorInfo(sid, new AsyncCallback<Map<String, String>>() {
            @Override
            public void onFailure(Throwable caught) {
                //TODO
            }

            @Override
            public void onSuccess(Map<String, String> result) {
                value = Double.parseDouble(result.get("value"));
            }
        });
    }

    public double getValue() {
        return value;
    }

    private native void createChart(String containerID, String chartID, String sensorName, double value, int interval, int sid) /*-{
        var that = this;
        var contentflow = $doc.getElementById(containerID);
        var barSpan = $doc.createElement("canvas");
        barSpan.id = chartID;
        barSpan.style = "width: 600px; height: 300px; float: left";
        contentflow.appendChild(barSpan);

        var barChart = new $wnd.Chart(barSpan, {
            type: 'bar',
            data: [0],
            options: {
                elements: {
                    line: {
                        tension: 0, // disables bezier curves
                    }
                }
            }
        });

        setInterval(function () {
            barChart.data.push(that.@client.assets.charts.LineChart::getValue()());
            barChart.update();
        }, interval);
    }-*/;

}
