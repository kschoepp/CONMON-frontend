package client.assets.dialogs;

import client.config.ZposConfig;
import client.locales.ErrorsEN;
import client.locales.MessagesEN;
import client.remote.SensorService;
import client.remote.SensorServiceAsync;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.HeadingSize;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.Pull;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyPlacement;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyType;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;
import org.gwtbootstrap3.extras.notify.client.ui.NotifySettings;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class AggregateBox extends DialogBox {

    //Local elements
    private Panel boxPanel;
    private Button addButton;
    private Button closeButton;
    //|---UI field elements
    private ListGroup aggregateList;
    //CONMON servlets
    private SensorServiceAsync sensorService;

    public AggregateBox() {
        super(false,false);
        this.setText("Active aggregates");
        this.getElement().getStyle().setZIndex(ZposConfig.getNextZpos());
        sensorService = SensorService.Instance.get();
        loadContent();
    }

    private void loadContent() {
        boxPanel = new Panel();
        loadBody();
        loadFooter();
        this.add(boxPanel);
    }

    private void loadBody() {
        PanelBody boxBody = new PanelBody();
        Heading ruleHeading = new Heading(HeadingSize.H4);
        ruleHeading.setText("Active aggregates");
        aggregateList = new ListGroup();
        aggregateList.setWidth("500px");
        boxBody.add(ruleHeading);
        boxBody.add(aggregateList);
        boxPanel.add(boxBody);
    }

    private void loadFooter() {
        PanelFooter boxFooter = new PanelFooter();
        addButton = new Button();
        addButton.setType(ButtonType.SUCCESS);
        addButton.setIcon(IconType.PLUS);
        closeButton = new Button();
        closeButton.setText("Close");
        closeButton.setPull(Pull.RIGHT);
        boxFooter.add(addButton);
        boxFooter.add(closeButton);
        boxPanel.add(boxFooter);
    }

    public void loadValues() {
        aggregateList.clear();
        ListGroupItem loadingItem = new ListGroupItem();
        loadingItem.setText("Loading...");
        aggregateList.add(loadingItem);
        sensorService.getActiveAggregates(new AsyncCallback<Map<Integer, String>>() {
            @Override
            public void onFailure(Throwable caught) {

            }

            @Override
            public void onSuccess(Map<Integer, String> result) {
                if(result.size() > 0) {
                    aggregateList.clear();
                    Set<Integer> keySet = result.keySet();
                    Iterator<Integer> keysIt = keySet.iterator();
                    while (keysIt.hasNext()) {
                        int curKey = keysIt.next();
                        ListGroupItem newItem = new ListGroupItem();
                        newItem.setText("Aggregate: " + result.get(curKey) + " | ID: " + curKey);
                        newItem.setId(Integer.toString(curKey));
                        Button deleteButton = new Button();
                        deleteButton.setIcon(IconType.TRASH);
                        deleteButton.setPull(Pull.RIGHT);
                        deleteButton.setType(ButtonType.DANGER);
                        deleteButton.setMarginLeft(10);
                        deleteButton.addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent event) {
                                sensorService.deleteAggregate(Integer.parseInt(newItem.getId()), new AsyncCallback<Integer>() {
                                    @Override
                                    public void onFailure(Throwable caught) {
                                        NotifySettings settings = NotifySettings.newSettings();
                                        settings.setType(NotifyType.DANGER);
                                        settings.setPlacement(NotifyPlacement.TOP_CENTER);
                                        Notify.notify(ErrorsEN.ERROR_AGGREGATE_NOT_REMOVED, settings);
                                    }

                                    @Override
                                    public void onSuccess(Integer result) {
                                        if(result == 1) {
                                            NotifySettings settings = NotifySettings.newSettings();
                                            settings.setType(NotifyType.SUCCESS);
                                            settings.setPlacement(NotifyPlacement.TOP_RIGHT);
                                            Notify.notify(MessagesEN.MESSAGE_AGGREGATE_REMOVED, settings);
                                            aggregateList.remove(newItem);
                                            loadValues();
                                        } else {
                                            NotifySettings settings = NotifySettings.newSettings();
                                            settings.setType(NotifyType.DANGER);
                                            settings.setPlacement(NotifyPlacement.TOP_CENTER);
                                            Notify.notify(ErrorsEN.ERROR_AGGREGATE_NOT_REMOVED, settings);
                                        }
                                    }
                                });
                            }
                        });
                        newItem.add(deleteButton);
                        aggregateList.add(newItem);
                    }
                } else {
                    aggregateList.clear();
                    ListGroupItem noActiveFound = new ListGroupItem();
                    noActiveFound.setText("No active aggregates found!");
                    aggregateList.add(noActiveFound);
                }
            }
        });
    }

    //SETTER ClickHandler
    public void setAddButtonHandler(ClickHandler clickHandler) {
        addButton.addClickHandler(clickHandler);
    }

    public void setCloseButtonHandler(ClickHandler clickHandler) {
        closeButton.addClickHandler(clickHandler);
    }

}
