package client.assets.dialogs;

import client.config.ZposConfig;
import com.google.gwt.dom.client.BRElement;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.DialogBox;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.IconType;

/**
 *  _____
 * |_   _|
 *   | |  _ __   __ _  ___ _ __   ___ ___
 *   | | | '_ \ / _` |/ _ \ '_ \ / __/ _ \
 *  _| |_| | | | (_| |  __/ | | | (_|  __/
 * |_____|_| |_|\__, |\___|_| |_|\___\___|
 *               __/ |
 *              |___/
 * https://ingence.de
 *
 * Created by Cedric Hopf on 08.09.2017
 * © https://github.com/CedricHopf
 **/
public class AddBox extends DialogBox {

    private final String BUTTONWIDTH = "200px";

    //Local elements
    private Panel boxPanel;
    private Button addRow;
    private Button addWidget;
    private Button closeButton;

    public AddBox() {
        super(false,false);
        this.setText("Add an element");
        this.getElement().getStyle().setZIndex(ZposConfig.getNextZpos());
        loadContent();
    }

    private void loadContent() {
        boxPanel = new Panel();
        loadBody();
        loadFooter();
        this.add(boxPanel);
    }

    private void loadBody() {
        PanelBody boxBody = new PanelBody();
        //Buttons
        addRow = new Button();
        addRow.setText("Add new row");
        addRow.setWidth(BUTTONWIDTH);
        addRow.setIcon(IconType.BARS);
        addWidget = new Button();
        addWidget.setText("Add new widget");
        addWidget.setWidth(BUTTONWIDTH);
        addWidget.setIcon(IconType.PIE_CHART);
        addWidget.setMarginTop(10);
        boxBody.add(addRow);
        boxBody.getElement().appendChild(DOM.createElement(BRElement.TAG));
        boxBody.add(addWidget);
        boxPanel.add(boxBody);
    }

    private void loadFooter() {
        PanelFooter boxFooter = new PanelFooter();
        closeButton = new Button();
        closeButton.setText("Close");
        boxFooter.add(closeButton);
        boxPanel.add(boxFooter);
    }

    public void setAddRowHandler(ClickHandler clickHandler) {
        addRow.addClickHandler(clickHandler);
    }

    public void setAddWidgetHandler(ClickHandler clickHandler) {
        addWidget.addClickHandler(clickHandler);
    }

    public void setCloseButtonHandler(ClickHandler clickHandler) {
        closeButton.addClickHandler(clickHandler);
    }

}
