package client.assets.dialogs;

import client.config.NotifyConfig;
import client.config.ZposConfig;
import client.locales.ErrorsEN;
import client.locales.MessagesEN;
import client.remote.SensorService;
import client.remote.SensorServiceAsync;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.HeadingSize;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.Pull;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyPlacement;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyType;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;
import org.gwtbootstrap3.extras.notify.client.ui.NotifySettings;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *  _____
 * |_   _|
 *   | |  _ __   __ _  ___ _ __   ___ ___
 *   | | | '_ \ / _` |/ _ \ '_ \ / __/ _ \
 *  _| |_| | | | (_| |  __/ | | | (_|  __/
 * |_____|_| |_|\__, |\___|_| |_|\___\___|
 *               __/ |
 *              |___/
 * https://ingence.de
 *
 * Created by Cedric Hopf on 08.09.2017
 * © https://github.com/CedricHopf
 **/
public class SensorBox extends DialogBox {

    //Local elements
    private Panel boxPanel;
    private Button addButton;
    private Button closeButton;
    //|---UI field elements
    private ListBox machineList;
    private ListGroup sensorList;
    //|---Datasave elements
    private Map<String,Integer> activeMachines;
    //CONMON servlets
    private SensorServiceAsync sensorService;

    public SensorBox() {
        super(false,false);
        this.setText("Active sensors");
        this.getElement().getStyle().setZIndex(ZposConfig.getNextZpos());
        sensorService = SensorService.Instance.get();
        activeMachines = new HashMap<>();
        loadContent();
    }

    private void loadContent() {
        boxPanel = new Panel();
        loadBody();
        loadFooter();
        this.add(boxPanel);
    }

    private void loadBody() {
        PanelBody boxBody = new PanelBody();
        Heading machineBoxHeading = new Heading(HeadingSize.H4);
        machineBoxHeading.setText("Select a machine");
        machineList = new ListBox();
        machineList.addItem("---");
        machineList.setWidth("500px");
        machineList.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                if(!machineList.getSelectedValue().equals("---")) {
                    String mname = machineList.getSelectedValue();
                    loadSensors(activeMachines.get(mname));
                } else {
                    resetSensorList();
                }
            }
        });
        Heading sensorListHeading = new Heading(HeadingSize.H4);
        sensorListHeading.setText("Active sensors by selected machine");
        sensorList = new ListGroup();
        sensorList.setWidth("500px");
        ListGroupItem selectMachinesItem = new ListGroupItem();
        selectMachinesItem.setText("Please select a machine");
        sensorList.add(selectMachinesItem);
        boxBody.add(machineBoxHeading);
        boxBody.add(machineList);
        boxBody.add(sensorListHeading);
        boxBody.add(sensorList);
        boxPanel.add(boxBody);
    }

    private void loadFooter() {
        PanelFooter boxFooter = new PanelFooter();
        addButton = new Button();
        addButton.setType(ButtonType.SUCCESS);
        addButton.setIcon(IconType.PLUS);
        closeButton = new Button("Close");
        closeButton.setPull(Pull.RIGHT);
        boxFooter.add(addButton);
        boxFooter.add(closeButton);
        boxPanel.add(boxFooter);
    }

    private void resetSensorList() {
        sensorList.clear();
        ListGroupItem selectMachinesItem = new ListGroupItem();
        selectMachinesItem.setText("Please select a machine");
        sensorList.add(selectMachinesItem);
    }

    public void loadValues() {
        machineList.clear();
        activeMachines.clear();
        machineList.addItem("loading...");
        machineList.setEnabled(false);
        sensorService.getActiveMachines(new AsyncCallback<Map<Integer, String>>() {
            @Override
            public void onFailure(Throwable caught) {
                Notify.notify(ErrorsEN.ERROR_RPC, NotifyConfig.notifyError());
            }

            @Override
            public void onSuccess(Map<Integer, String> result) {
                Set<Integer> keySet = result.keySet();
                Iterator<Integer> keysIt = keySet.iterator();
                machineList.clear();
                machineList.addItem("---");
                while(keysIt.hasNext()) {
                    int key = keysIt.next();
                    String value = result.get(key);
                    machineList.addItem(value);
                    activeMachines.put(value,key);
                }
                machineList.setSelectedIndex(0);
                machineList.setEnabled(true);
            }
        });
        resetSensorList();
    }

    private void loadSensors(int mid) {
        sensorList.clear();
        ListGroupItem loadingItem = new ListGroupItem();
        loadingItem.setText("loading...");
        sensorList.add(loadingItem);

        sensorService.getActiveSensorsByMachine(mid, new AsyncCallback<Map<Integer, String>>() {
            @Override
            public void onFailure(Throwable caught) {
                Notify.notify(ErrorsEN.ERROR_RPC, NotifyConfig.notifyError());
            }

            @Override
            public void onSuccess(Map<Integer, String> result) {
                sensorList.clear();
                if(!result.isEmpty()) {
                    Set<Integer> keySet = result.keySet();
                    Iterator<Integer> keysIt = keySet.iterator();
                    while(keysIt.hasNext()) {
                        int curKey = keysIt.next();
                        ListGroupItem newItem = new ListGroupItem();
                        newItem.setText(result.get(curKey));
                        newItem.setId(Integer.toString(curKey));
                        Button deleteButton = new Button();
                        deleteButton.setIcon(IconType.TRASH);
                        deleteButton.setPull(Pull.RIGHT);
                        deleteButton.setType(ButtonType.DANGER);
                        deleteButton.setMarginLeft(10);
                        deleteButton.addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent event) {
                                sensorService.deleteSensor(Integer.parseInt(newItem.getId()), new AsyncCallback<Integer>() {
                                    @Override
                                    public void onFailure(Throwable caught) {
                                        //TODO
                                    }

                                    @Override
                                    public void onSuccess(Integer result) {
                                        if(result == 1) {
                                            NotifySettings settings = NotifySettings.newSettings();
                                            settings.setType(NotifyType.SUCCESS);
                                            settings.setPlacement(NotifyPlacement.TOP_RIGHT);
                                            Notify.notify(MessagesEN.MESSAGE_SENSOR_REMOVED, settings);
                                            sensorList.remove(newItem);
                                            loadValues();
                                            resetSensorList();
                                        } else {
                                            NotifySettings settings = NotifySettings.newSettings();
                                            settings.setType(NotifyType.DANGER);
                                            settings.setPlacement(NotifyPlacement.TOP_CENTER);
                                            Notify.notify(ErrorsEN.ERROR_SENSOR_NOT_REMOVED, settings);
                                        }
                                    }
                                });
                            }
                        });
                        newItem.add(deleteButton);
                        sensorList.add(newItem);
                    }
                } else {
                    ListGroupItem noActiveSensors = new ListGroupItem();
                    noActiveSensors.setText("No active sensors found!");
                    sensorList.add(noActiveSensors);
                }
            }
        });
    }

    public void setAddButtonHandler(ClickHandler clickHandler) {
        addButton.addClickHandler(clickHandler);
    }

    public void setCloseButtonHandler(ClickHandler clickHandler) {
        closeButton.addClickHandler(clickHandler);
    }

}
