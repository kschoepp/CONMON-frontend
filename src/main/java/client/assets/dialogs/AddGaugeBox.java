package client.assets.dialogs;

import client.config.NotifyConfig;
import client.config.ZposConfig;
import client.locales.ErrorsEN;
import client.remote.SensorService;
import client.remote.SensorServiceAsync;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.InputType;
import org.gwtbootstrap3.client.ui.constants.Pull;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *  _____
 * |_   _|
 *   | |  _ __   __ _  ___ _ __   ___ ___
 *   | | | '_ \ / _` |/ _ \ '_ \ / __/ _ \
 *  _| |_| | | | (_| |  __/ | | | (_|  __/
 * |_____|_| |_|\__, |\___|_| |_|\___\___|
 *               __/ |
 *              |___/
 * https://ingence.de
 *
 * Created by Cedric Hopf on 25.08.2017
 * © https://github.com/CedricHopf
 **/
public class AddGaugeBox extends DialogBox {

    //UI Elements
    private Panel boxPanel;
    private Button createButton;
    private Button cancelButton;
    //|--- UI value fields
    private ListBox selectSensor;
    private ListBox selectRow;
    private Map<Integer,String> sensorMap;
    private Input minInput;
    private Input maxInput;
    //Servlets
    SensorServiceAsync sensorService;


    //Constructor
    public AddGaugeBox() {
        super(false, false);
        this.setText("Add Gauge Chart");
        this.getElement().getStyle().setZIndex(ZposConfig.getNextZpos());
        sensorService = SensorService.Instance.get();
        sensorMap = new HashMap<>();
        loadContent();
    }

    private void loadContent() {
        boxPanel = new Panel();
        loadBody();
        loadFooter();
        this.add(boxPanel);
    }

    private void loadBody() {
        PanelBody boxBody = new PanelBody();
        //|--- ROW 0
        FormGroup selectRowGroup = new FormGroup();
        FormLabel selectRowLabel = new FormLabel();
        selectRowLabel.setText("Select a row");
        selectRow = new ListBox();
        selectRow.addItem("no rows found!");
        selectRowGroup.add(selectRowLabel);
        selectRowGroup.add(selectRow);
        boxBody.add(selectRowGroup);
        //|--- ROW 1
        FormGroup selectSensorGroup = new FormGroup();
        FormLabel selectSensorLabel = new FormLabel();
        selectSensorLabel.setText("Select a sensor");
        selectSensor = new ListBox();
        selectSensor.addItem("no active sensors found!");
        selectSensorGroup.add(selectSensorLabel);
        selectSensorGroup.add(selectSensor);
        boxBody.add(selectSensorGroup);
        //|--- ROW 2
        FormGroup setMinGroup = new FormGroup();
        FormLabel setMin = new FormLabel();
        setMin.setText("Gauge min value");
        minInput = new Input();
        minInput.setType(InputType.NUMBER);
        minInput.setPlaceholder("e.g. 0");
        FormGroup setMaxGroup = new FormGroup();
        FormLabel setMax = new FormLabel();
        setMax.setText("Gauge max value");
        maxInput = new Input();
        maxInput.setType(InputType.NUMBER);
        maxInput.setPlaceholder("e.g. 100");
        setMinGroup.add(setMin);
        setMinGroup.add(minInput);
        setMaxGroup.add(setMax);
        setMaxGroup.add(maxInput);
        boxBody.add(setMinGroup);
        boxBody.add(setMaxGroup);
        //adding body to panel
        boxPanel.add(boxBody);
    }

    private void loadFooter() {
        PanelFooter boxFooter = new PanelFooter();
        createButton = new Button("Create");
        createButton.setType(ButtonType.SUCCESS);
        cancelButton = new Button("Cancel");
        cancelButton.setPull(Pull.RIGHT);
        boxFooter.add(createButton);

        boxFooter.add(cancelButton);
        boxPanel.add(boxFooter);
    }

    public void loadValues(Map<String,String> rows) {
        //Select Row LISTBOX
        selectRow.clear();
        selectRow.setEnabled(true);
        selectRow.addItem("---");
        if(rows.size() > 0) {
            Set<String> rowSet = rows.keySet();
            Iterator<String> rowIt = rowSet.iterator();
            while (rowIt.hasNext()) {
                selectRow.addItem(rowIt.next());
            }
            selectRow.setSelectedIndex(0);
        } else {
            selectRow.clear();
            selectRow.addItem("No rows created!");
            selectRow.setSelectedIndex(0);
            selectRow.setEnabled(false);
        }
        //Select Sensor LISTBOX
        selectSensor.clear();
        selectSensor.addItem("Loading...");
        selectSensor.setEnabled(false);
        sensorService.getActiveSensors(new AsyncCallback<Map<Integer, String>>() {
            @Override
            public void onFailure(Throwable caught) {
                Notify.notify(ErrorsEN.ERROR_RPC, NotifyConfig.notifyError());
                selectSensor.removeItem(0);
                selectSensor.addItem("RPC error");
            }

            @Override
            public void onSuccess(Map<Integer, String> result) {
                selectSensor.removeItem(0);
                selectSensor.addItem("---");
                if(result.size() != 0) {
                    sensorMap = result;
                    Set<Integer> keys = result.keySet();
                    Iterator<Integer> keysIt = keys.iterator();
                    while(keysIt.hasNext()) {
                        selectSensor.addItem(result.get(keysIt.next()));
                    }
                    selectSensor.setEnabled(true);
                } else {
                    selectSensor.clear();
                    selectSensor.addItem("no active sensors found!");
                }
                selectSensor.setSelectedIndex(0);

            }
        });
    }

    public void setCreateButtonHandler(ClickHandler clickHandler) {
        createButton.addClickHandler(clickHandler);
    }

    public void setCancelButtonHandler(ClickHandler clickHandler) {
        cancelButton.addClickHandler(clickHandler);
    }

    public int getSelectedSensorID() {
        String sensorName = selectSensor.getSelectedValue();
        if(sensorMap.isEmpty()) {
            return -3;
        } else if(sensorName.equals("---")) {
            return -1;
        } else {
            Set<Integer> keys = sensorMap.keySet();
            Iterator<Integer> keysIt = keys.iterator();
            while (keysIt.hasNext()) {
                int curKey = keysIt.next();
                if (sensorMap.get(curKey).equals(sensorName)) {
                    return curKey;
                }
            }
        }
        return -1;
    }

    public String getSelectedRow() {
        String rowName = selectRow.getSelectedValue();
        if(rowName.equals("---")) {
            return "err-3";
        } else {
            return rowName;
        }
    }

    public double getMin() {
        try {
            return Double.parseDouble(minInput.getValue());
        } catch (Exception e) {
            return -1;
        }
    }

    public double getMax() {
        try {
            return Double.parseDouble(maxInput.getValue());
        } catch (Exception e) {
            return -1;
        }
    }

}
