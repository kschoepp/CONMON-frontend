package client.assets.dialogs;

import client.assets.AssetConstants;
import client.config.ZposConfig;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.DialogBox;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.*;

/**
 *  _____
 * |_   _|
 *   | |  _ __   __ _  ___ _ __   ___ ___
 *   | | | '_ \ / _` |/ _ \ '_ \ / __/ _ \
 *  _| |_| | | | (_| |  __/ | | | (_|  __/
 * |_____|_| |_|\__, |\___|_| |_|\___\___|
 *               __/ |
 *              |___/
 * https://ingence.de
 *
 * Created by Cedric Hopf on 25.08.2017
 * © https://github.com/CedricHopf
 **/
public class AddWidgetBox extends DialogBox {

    //Local elements
    private Panel boxPanel;
    private Button addButton;
    private Button cancelButton;
    //|--- UI value fields;
    private ButtonGroup widgets;
    private RadioButton gaugeChart;
    private RadioButton lineChart;
    private RadioButton barChart;

    public AddWidgetBox() {
        super(false,false);
        this.setText("Add new widget");
        this.getElement().getStyle().setZIndex(ZposConfig.getNextZpos());
        loadContent();
    }

    private void loadContent() {
        boxPanel = new Panel();
        loadBody();
        loadFooter();
        this.add(boxPanel);
    }

    private void loadBody() {
        PanelBody boxBody = new PanelBody();
        widgets = new ButtonGroup();
        widgets.setDataToggle(Toggle.BUTTONS);
        gaugeChart = new RadioButton("Gauge Chart");
        gaugeChart.setIcon(IconType.TACHOMETER);
        gaugeChart.setIconSize(IconSize.TIMES2);
        lineChart = new RadioButton("Line Chart");
        lineChart.setIcon(IconType.LINE_CHART);
        lineChart.setIconSize(IconSize.TIMES2);
        barChart = new RadioButton("Bar Chart");
        barChart.setIcon(IconType.BAR_CHART);
        barChart.setIconSize(IconSize.TIMES2);
        widgets.add(gaugeChart);
        widgets.add(lineChart);
        widgets.add(barChart);
        boxBody.add(widgets);
        boxPanel.add(boxBody);
    }

    private void loadFooter() {
        PanelFooter boxFooter = new PanelFooter();
        addButton = new Button("Add");
        addButton.setType(ButtonType.SUCCESS);
        cancelButton = new Button("Cancel");
        cancelButton.setPull(Pull.RIGHT);
        boxFooter.add(addButton);
        boxFooter.add(cancelButton);
        boxPanel.add(boxFooter);
    }

    public void reload() {
        gaugeChart.setActive(false);
        lineChart.setActive(false);
        barChart.setActive(false);
    }

    public int getSelectChart() {
        if(gaugeChart.isActive()) {
            return AssetConstants.SELECT_GAUGECHART;
        } else if (lineChart.isActive()) {
            return AssetConstants.SELECT_LINECHART;
        } else if (barChart.isActive()) {
            return AssetConstants.SELECT_BARCHART;
        } else {
            return AssetConstants.SELECT_NOTHING;
        }
    }

    public void setAddButtonHandler(ClickHandler clickHandler) {
        addButton.addClickHandler(clickHandler);
    }

    public void setCancelButton(ClickHandler clickHandler) {
        cancelButton.addClickHandler(clickHandler);
    }

}
