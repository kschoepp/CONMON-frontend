package client.assets.dialogs;

import client.config.ConfigService;
import client.config.ConfigServiceAsync;
import client.config.ZposConfig;
import com.google.gwt.dom.client.BRElement;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.DialogBox;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.IconType;

/**
 *  _____
 * |_   _|
 *   | |  _ __   __ _  ___ _ __   ___ ___
 *   | | | '_ \ / _` |/ _ \ '_ \ / __/ _ \
 *  _| |_| | | | (_| |  __/ | | | (_|  __/
 * |_____|_| |_|\__, |\___|_| |_|\___\___|
 *               __/ |
 *              |___/
 * https://ingence.de
 *
 * Created by Cedric Hopf on 08.09.2017
 * © https://github.com/CedricHopf
 **/
public class ControlPanel extends DialogBox {

    private final String BUTTONWIDTH = "200px";

    //Local elements
    private Panel boxPanel;
    private Button closeButton;
    //|---UI fields buttons
    private Button machineButton;
    private Button sensorButton;
    private Button ruleButton;
    private Button aggregateButton;
    private Button addEventButton;

    public ControlPanel() {
        super(false,false);
        this.setText("Control panel");
        this.getElement().getStyle().setZIndex(ZposConfig.getNextZpos());
        loadContent();
    }

    private void loadContent() {
        boxPanel = new Panel();
        loadBody();
        loadFooter();
        this.add(boxPanel);
    }

    private void loadBody() {
        PanelBody boxBody = new PanelBody();
        //|---Setting Dialog buttons
        machineButton = new Button();
        machineButton.setText("Manage machines");
        machineButton.setIcon(IconType.INDUSTRY);
        machineButton.setWidth(BUTTONWIDTH);
        sensorButton = new Button();
        sensorButton.setText("Manage sensors");
        sensorButton.setIcon(IconType.WRENCH);
        sensorButton.setWidth(BUTTONWIDTH);
        sensorButton.setMarginTop(10);
        ruleButton = new Button();
        ruleButton.setText("Manage rules");
        ruleButton.setIcon(IconType.FILTER);
        ruleButton.setWidth(BUTTONWIDTH);
        ruleButton.setMarginTop(10);
        aggregateButton = new Button();
        aggregateButton.setText("Manage aggregates");
        aggregateButton.setIcon(IconType.FILTER);
        aggregateButton.setWidth(BUTTONWIDTH);
        aggregateButton.setMarginTop(10);
        addEventButton = new Button();
        addEventButton.setText("Add new event");
        addEventButton.setIcon(IconType.PLUS);
        addEventButton.setWidth(BUTTONWIDTH);
        addEventButton.setMarginTop(10);
        boxBody.add(machineButton);
        boxBody.getElement().appendChild(DOM.createElement(BRElement.TAG));
        boxBody.add(sensorButton);
        boxBody.getElement().appendChild(DOM.createElement(BRElement.TAG));
        boxBody.add(ruleButton);
        boxBody.getElement().appendChild(DOM.createElement(BRElement.TAG));
        boxBody.add(aggregateButton);
        boxBody.getElement().appendChild(DOM.createElement(BRElement.TAG));
        boxBody.add(addEventButton);
        boxPanel.add(boxBody);
    }

    private void loadFooter() {
        PanelFooter boxFooter = new PanelFooter();
        closeButton = new Button();
        closeButton.setText("Close");
        boxFooter.add(closeButton);
        boxPanel.add(boxFooter);
    }

    public void setMachineButtonHandler(ClickHandler clickHandler) {
        machineButton.addClickHandler(clickHandler);
    }

    public void setSensorButtonHandler(ClickHandler clickHandler) {
        sensorButton.addClickHandler(clickHandler);
    }

    public void setRuleButtonHandler(ClickHandler clickHandler) {
        ruleButton.addClickHandler(clickHandler);
    }

    public void setAggregateButtonHandler(ClickHandler clickHandler) {
        aggregateButton.addClickHandler(clickHandler);
    }

    public void setCloseButtonHandler(ClickHandler clickHandler) {
        closeButton.addClickHandler(clickHandler);
    }

    public void setAddEventButtonHandler(ClickHandler clickHandler) {
        addEventButton.addClickHandler(clickHandler);
    }

}
