package client.assets.dialogs;

import client.config.ZposConfig;
import client.locales.ErrorsEN;
import client.locales.MessagesEN;
import client.remote.SensorService;
import client.remote.SensorServiceAsync;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.HeadingSize;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.Pull;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyPlacement;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyType;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;
import org.gwtbootstrap3.extras.notify.client.ui.NotifySettings;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *  _____
 * |_   _|
 *   | |  _ __   __ _  ___ _ __   ___ ___
 *   | | | '_ \ / _` |/ _ \ '_ \ / __/ _ \
 *  _| |_| | | | (_| |  __/ | | | (_|  __/
 * |_____|_| |_|\__, |\___|_| |_|\___\___|
 *               __/ |
 *              |___/
 * https://ingence.de
 *
 * Created by Cedric Hopf on 17.09.2017
 * © https://github.com/CedricHopf
 **/
public class RuleBox extends DialogBox {

    //Local elements
    private Panel boxPanel;
    private Button addButton;
    private Button closeButton;
    //|---UI field elements
    private ListGroup rulesList;
    //CONMON servlets
    private SensorServiceAsync sensorService;

    public RuleBox() {
        super(false,false);
        this.setText("Active rules");
        this.getElement().getStyle().setZIndex(ZposConfig.getNextZpos());
        sensorService = SensorService.Instance.get();
        loadContent();
    }

    private void loadContent() {
        boxPanel = new Panel();
        loadBody();
        loadFooter();
        this.add(boxPanel);
    }

    private void loadBody() {
        PanelBody boxBody = new PanelBody();
        Heading ruleHeading = new Heading(HeadingSize.H4);
        ruleHeading.setText("Active rules");
        rulesList = new ListGroup();
        rulesList.setWidth("500px");
        boxBody.add(ruleHeading);
        boxBody.add(rulesList);
        boxPanel.add(boxBody);
    }

    private void loadFooter() {
        PanelFooter boxFooter = new PanelFooter();
        addButton = new Button();
        addButton.setType(ButtonType.SUCCESS);
        addButton.setIcon(IconType.PLUS);
        closeButton = new Button();
        closeButton.setText("Close");
        closeButton.setPull(Pull.RIGHT);
        boxFooter.add(addButton);
        boxFooter.add(closeButton);
        boxPanel.add(boxFooter);
    }

    public void loadValues() {
        rulesList.clear();
        ListGroupItem loadingItem = new ListGroupItem();
        loadingItem.setText("Loading...");
        rulesList.add(loadingItem);
        sensorService.getActiveRules(new AsyncCallback<Map<Integer, String>>() {
            @Override
            public void onFailure(Throwable caught) {

            }

            @Override
            public void onSuccess(Map<Integer, String> result) {
                if(result.size() > 0) {
                    rulesList.clear();
                    Set<Integer> keySet = result.keySet();
                    Iterator<Integer> keysIt = keySet.iterator();
                    while (keysIt.hasNext()) {
                        int curKey = keysIt.next();
                        ListGroupItem newItem = new ListGroupItem();
                        newItem.setText("Rule: " + result.get(curKey) + " | ID: " + curKey);
                        newItem.setId(Integer.toString(curKey));
                        Button deleteButton = new Button();
                        deleteButton.setIcon(IconType.TRASH);
                        deleteButton.setPull(Pull.RIGHT);
                        deleteButton.setType(ButtonType.DANGER);
                        deleteButton.setMarginLeft(10);
                        deleteButton.addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent event) {
                                sensorService.deleteRule(Integer.parseInt(newItem.getId()), new AsyncCallback<Integer>() {
                                    @Override
                                    public void onFailure(Throwable caught) {
                                        NotifySettings settings = NotifySettings.newSettings();
                                        settings.setType(NotifyType.DANGER);
                                        settings.setPlacement(NotifyPlacement.TOP_CENTER);
                                        Notify.notify(ErrorsEN.ERROR_RULE_NOT_REMOVED, settings);
                                    }

                                    @Override
                                    public void onSuccess(Integer result) {
                                        if(result == 1) {
                                            NotifySettings settings = NotifySettings.newSettings();
                                            settings.setType(NotifyType.SUCCESS);
                                            settings.setPlacement(NotifyPlacement.TOP_RIGHT);
                                            Notify.notify(MessagesEN.MESSAGE_RULE_REMOVED, settings);
                                            rulesList.remove(newItem);
                                            loadValues();
                                        } else {
                                            NotifySettings settings = NotifySettings.newSettings();
                                            settings.setType(NotifyType.DANGER);
                                            settings.setPlacement(NotifyPlacement.TOP_CENTER);
                                            Notify.notify(ErrorsEN.ERROR_RULE_NOT_REMOVED, settings);
                                        }
                                    }
                                });
                            }
                        });
                        newItem.add(deleteButton);
                        rulesList.add(newItem);
                    }
                } else {
                    rulesList.clear();
                    ListGroupItem noActiveFound = new ListGroupItem();
                    noActiveFound.setText("No active rules found!");
                    rulesList.add(noActiveFound);
                }
            }
        });
    }

    //SETTER ClickHandler
    public void setAddButtonHandler(ClickHandler clickHandler) {
        addButton.addClickHandler(clickHandler);
    }

    public void setCloseButtonHandler(ClickHandler clickHandler) {
        closeButton.addClickHandler(clickHandler);
    }

}
