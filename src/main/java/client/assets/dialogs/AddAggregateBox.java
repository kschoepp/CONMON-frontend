package client.assets.dialogs;

import client.config.ZposConfig;
import client.locales.ErrorsEN;
import client.locales.MessagesEN;
import client.remote.SensorService;
import client.remote.SensorServiceAsync;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.*;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyPlacement;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyType;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;
import org.gwtbootstrap3.extras.notify.client.ui.NotifySettings;

public class AddAggregateBox extends DialogBox {

    //Global variables

    //|---UI elements
    private Panel boxPanel;
    private PanelBody boxBody;
    private PanelFooter boxFooter;
    private FormGroup aggregateBlock;
    private Button addButton;
    private Button plusButton;
    private Button cancelButton;
    //CONMON servlets
    private SensorServiceAsync sensorService;

    //Constructor
    public AddAggregateBox() {
        super(false,false);
        this.setText("Add new aggregate");
        this.getElement().getStyle().setZIndex(ZposConfig.getNextZpos());
        sensorService = SensorService.Instance.get();
        loadContent();
        loadHandlers();
    }

    private void loadContent() {
        boxPanel = new Panel();
        loadBody();
        loadFooter();
        this.add(boxPanel);
    }

    private void loadBody() {
        boxBody = new PanelBody();
        Heading aggregateHeading = new Heading(HeadingSize.H4);
        aggregateHeading.setText("Define a new aggregate");
        boxBody.add(aggregateHeading);
        aggregateBlock = new FormGroup();
        aggregateBlock.setSize(FormGroupSize.LARGE);
        aggregateBlock.add(newParaminput());
        aggregateBlock.add(newOperatorList());
        boxBody.add(aggregateBlock);
        plusButton = new Button();
        plusButton.setIcon(IconType.PLUS);
        plusButton.setType(ButtonType.SUCCESS);
        plusButton.setPull(Pull.RIGHT);
        boxBody.add(plusButton);
        boxPanel.add(boxBody);
    }

    private void loadFooter() {
        boxFooter = new PanelFooter();
        addButton = new Button("Add");
        addButton.setType(ButtonType.SUCCESS);
        cancelButton = new Button("Cancel");
        cancelButton.setPull(Pull.RIGHT);
        boxFooter.add(addButton);
        boxFooter.add(cancelButton);
        //adding footer to panel
        boxPanel.add(boxFooter);
    }

    private Input newParaminput() {
        Input output = new Input();
        output.setPlaceholder("e.g. A1, s1, etc");
        output.setType(InputType.TEXT);
        return output;
    }

    private ListBox newOperatorList() {
        ListBox output = new ListBox();
        output.addItem("END");
        output.addItem("-");
        output.addItem("+");
        output.addItem("%");
        output.addItem("*");
        output.addItem("^");
        return output;
    }

    private void loadHandlers() {
        addButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                sensorService.getNextAID(new AsyncCallback<Integer>() {
                    @Override
                    public void onFailure(Throwable caught) {

                    }

                    @Override
                    public void onSuccess(Integer result) {
                        int aid = result;
                        int aggregateEntries = aggregateBlock.getWidgetCount();
                        int subaid = 1;
                        int i = 0;
                        String sensorUsed = ";";
                        while(i < aggregateEntries) {
                            Input curInput = (Input) aggregateBlock.getWidget(i);
                            if(curInput.getValue().contains("s")) {
                                sensorUsed = sensorUsed + curInput.getValue() + ";";
                            }
                            i = i+2;
                        }
                        i = 0;
                        while(i < aggregateEntries) {
                            Input paramWidget = (Input) aggregateBlock.getWidget(i);
                            String param = paramWidget.getValue();
                            i++;
                            ListBox operatorWidget = (ListBox) aggregateBlock.getWidget(i);
                            String operator = operatorWidget.getSelectedValue();
                            i++;
                            sensorService.addAggregate(aid, subaid, "Aggregate" + aid, param, operator, sensorUsed, new AsyncCallback<Integer>() {
                                @Override
                                public void onFailure(Throwable caught) {
                                    NotifySettings settings = NotifySettings.newSettings();
                                    settings.setType(NotifyType.DANGER);
                                    settings.setPlacement(NotifyPlacement.TOP_CENTER);
                                    Notify.notify(ErrorsEN.ERROR_AGGREGATE_NOT_ADDED, settings);
                                }

                                @Override
                                public void onSuccess(Integer result) {
                                    if(result == 200) {
                                        NotifySettings settings = NotifySettings.newSettings();
                                        settings.setType(NotifyType.SUCCESS);
                                        settings.setPlacement(NotifyPlacement.TOP_RIGHT);
                                        Notify.notify(MessagesEN.MESSAGE_AGGREGATE_ADDED, settings);
                                    } else {
                                        NotifySettings settings = NotifySettings.newSettings();
                                        settings.setType(NotifyType.DANGER);
                                        settings.setPlacement(NotifyPlacement.TOP_CENTER);
                                        Notify.notify(ErrorsEN.ERROR_AGGREGATE_NOT_ADDED, settings);
                                    }
                                }
                            });
                            subaid++;
                        }
                    }
                });
            }
        });
        plusButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                aggregateBlock.add(newParaminput());
                aggregateBlock.add(newOperatorList());
            }
        });
        cancelButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                AddAggregateBox.this.hide();
            }
        });
    }

}
