package client.assets.dialogs;

import client.config.ZposConfig;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.DialogBox;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.InputType;
import org.gwtbootstrap3.client.ui.constants.Pull;

public class AddRowBox extends DialogBox {

    //Global variables
    private Panel boxPanel;
    private Input rowNameInput;
    private Button addButton;
    private Button cancelButton;

    public AddRowBox() {
        super(false,false);
        this.setText("Add new row");
        this.getElement().getStyle().setZIndex(ZposConfig.getNextZpos());
        loadContent();
    }

    private void loadContent() {
        boxPanel = new Panel();
        loadBody();
        loadFooter();
        this.add(boxPanel);
    }

    private void loadBody() {
        PanelBody boxBody = new PanelBody();
        FormGroup rowNameGroup = new FormGroup();
        rowNameGroup.setWidth("300px");
        FormLabel rowNameLabel = new FormLabel();
        rowNameLabel.setText("Row name");
        rowNameInput = new Input();
        rowNameInput.setPlaceholder("e.g. Machine Container 1");
        rowNameInput.setType(InputType.TEXT);
        rowNameGroup.add(rowNameLabel);
        rowNameGroup.add(rowNameInput);
        boxBody.add(rowNameGroup);
        boxPanel.add(boxBody);
    }

    private void loadFooter() {
        PanelFooter boxFooter = new PanelFooter();
        addButton = new Button();
        addButton.setType(ButtonType.SUCCESS);
        addButton.setIcon(IconType.PLUS);
        cancelButton = new Button("Cancel");
        cancelButton.setPull(Pull.RIGHT);
        boxFooter.add(addButton);
        boxFooter.add(cancelButton);
        boxPanel.add(boxFooter);
    }

    public void reload() {
        rowNameInput.setValue("");
    }

    public String getRowName() {
        return rowNameInput.getValue();
    }

    public void setAddButtonHandler(ClickHandler clickHandler) {
        addButton.addClickHandler(clickHandler);
    }

    public void setCancelButton(ClickHandler clickHandler) {
        cancelButton.addClickHandler(clickHandler);
    }

}
