package client.assets.dialogs;

import client.config.NotifyConfig;
import client.config.ZposConfig;
import client.remote.SensorService;
import client.remote.SensorServiceAsync;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.*;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;
import org.gwtbootstrap3.extras.toggleswitch.client.ui.ToggleSwitch;

import java.nio.channels.AsynchronousByteChannel;

public class AddEventBox extends DialogBox {

    //Global variables;
    private Panel boxPanel;
    private PanelBody boxBody;
    private PanelFooter boxFooter;
    private Input rid;
    private Input eventType;
    private Input eventMessage;
    private ToggleSwitch triggerValue;
    private Button saveButton;
    private Button cancelButton;
    //CONMON servlets
    private SensorServiceAsync sensorService;

    //Constructor
    public AddEventBox() {
        super(false,false);
        this.setText("Add new event");
        this.getElement().getStyle().setZIndex(ZposConfig.getNextZpos());
        sensorService = SensorService.Instance.get();
        loadContent();
        loadHandlers();
    }

    private void loadContent() {
        boxPanel = new Panel();
        loadBody();
        loadFooter();
        this.add(boxPanel);
    }

    private void loadBody() {
        boxBody = new PanelBody();
        Heading eventHeading = new Heading(HeadingSize.H4);
        eventHeading.setText("Define a new event");
        boxBody.add(eventHeading);
        FormGroup eventGroup = new FormGroup();
        eventGroup.setSize(FormGroupSize.LARGE);
        rid = new Input();
        rid.setType(InputType.NUMBER);
        eventGroup.add(rid);
        eventType = new Input();
        eventType.setType(InputType.TEXT);
        eventGroup.add(eventType);
        eventMessage = new Input();
        eventMessage.setType(InputType.TEXT);
        eventGroup.add(eventMessage);
        triggerValue = new ToggleSwitch();
        triggerValue.setOnIcon(IconType.CHECK);
        triggerValue.setOffIcon(IconType.TIMES);
        eventGroup.add(triggerValue);
        boxBody.add(eventGroup);
        boxPanel.add(boxBody);
    }

    private void loadFooter() {
        boxFooter = new PanelFooter();
        saveButton = new Button("Save");
        saveButton.setType(ButtonType.SUCCESS);
        cancelButton = new Button("Cancel");
        cancelButton.setPull(Pull.RIGHT);
        boxFooter.add(saveButton);
        boxFooter.add(cancelButton);
        boxPanel.add(boxFooter);
    }

    private void loadHandlers() {
        cancelButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                AddEventBox.this.hide();
            }
        });
        saveButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                sensorService.addEvent(Integer.parseInt(rid.getValue()), eventType.getValue(), eventMessage.getValue(), triggerValue.getValue(), new AsyncCallback<Integer>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        Notify.notify("Error", NotifyConfig.notifyError());
                    }

                    @Override
                    public void onSuccess(Integer result) {
                        if(result == 200) {
                            Notify.notify("Success", NotifyConfig.notifyError());
                            AddEventBox.this.hide();
                        } else {
                            Notify.notify("Error", NotifyConfig.notifyError());
                        }
                    }
                });
            }
        });
    }


}
