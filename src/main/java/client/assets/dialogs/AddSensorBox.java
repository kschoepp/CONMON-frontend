package client.assets.dialogs;

import client.config.ConfigService;
import client.config.ConfigServiceAsync;
import client.config.NotifyConfig;
import client.config.ZposConfig;
import client.locales.ErrorsEN;
import client.remote.SensorService;
import client.remote.SensorServiceAsync;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.sun.corba.se.impl.logging.InterceptorsSystemException;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.InputType;
import org.gwtbootstrap3.client.ui.constants.Pull;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *  _____
 * |_   _|
 *   | |  _ __   __ _  ___ _ __   ___ ___
 *   | | | '_ \ / _` |/ _ \ '_ \ / __/ _ \
 *  _| |_| | | | (_| |  __/ | | | (_|  __/
 * |_____|_| |_|\__, |\___|_| |_|\___\___|
 *               __/ |
 *              |___/
 * https://ingence.de
 *
 * Created by Cedric Hopf on 10.09.2017
 * © https://github.com/CedricHopf
 **/
public class AddSensorBox extends DialogBox {

    private static final String FORMWIDTH = "300px";

    //Global variables
    private Panel boxPanel;
    private Button addButton;
    private Button cancelButton;
    private Map<String,Integer> machines;
    //|---UI field elements
    private FormLabel sensorNameLabel;
    private FormLabel machineListLabel;
    private FormLabel nameSpaceLabel;
    private FormLabel nodeidLabel;
    private FormLabel valuetypeLabel;
    private FormLabel deltaLabel;
    private FormLabel unitLabel;
    private Input sensornameInput;
    private ListBox machineListBox;
    private Input namespaceInput;
    private Input nodeidInput;
    private ListBox valueTypeBox;
    private Input deltaInput;
    private Input unitInput;
    //CONMON Servlets
    private ConfigServiceAsync configService;
    private SensorServiceAsync sensorService;

    //Constructor
    public AddSensorBox() {
        super(false,false);
        this.setText("Add new sensor");
        this.getElement().getStyle().setZIndex(ZposConfig.getNextZpos());
        this.machines = new HashMap<>();
        configService = ConfigService.Instance.get();
        sensorService = SensorService.Instance.get();
        loadContent();
    }

    private void loadContent() {
        boxPanel = new Panel();
        loadBody();
        loadFooter();
        this.add(boxPanel);
    }

    private void loadBody() {
        PanelBody boxBody = new PanelBody();
        //ROW 1
        FormGroup sensorNameGroup = new FormGroup();
        sensorNameGroup.setWidth(FORMWIDTH);
        sensorNameLabel = new FormLabel();
        sensorNameLabel.setText("Sensorname");
        sensornameInput = new Input();
        sensornameInput.setType(InputType.TEXT);
        sensornameInput.setPlaceholder("e.g. Sensor 1");
        sensorNameGroup.add(sensorNameLabel);
        sensorNameGroup.add(sensornameInput);
        boxBody.add(sensorNameGroup);
        //ROW 2
        FormGroup machineListGroup = new FormGroup();
        machineListGroup.setWidth(FORMWIDTH);
        machineListLabel = new FormLabel();
        machineListLabel.setText("Select a machine");
        machineListBox = new ListBox();
        machineListBox.addItem("---");
        machineListGroup.add(machineListLabel);
        machineListGroup.add(machineListBox);
        boxBody.add(machineListGroup);
        //ROW 3
        FormGroup nameSpaceGroup = new FormGroup();
        nameSpaceGroup.setWidth(FORMWIDTH);
        nameSpaceLabel = new FormLabel();
        nameSpaceLabel.setText("Namespace");
        namespaceInput = new Input();
        namespaceInput.setType(InputType.NUMBER);
        namespaceInput.setPlaceholder("e.g. 1");
        nameSpaceGroup.add(nameSpaceLabel);
        nameSpaceGroup.add(namespaceInput);
        boxBody.add(nameSpaceGroup);
        //ROW 4
        FormGroup nodeidGroup = new FormGroup();
        nodeidGroup.setWidth(FORMWIDTH);
        nodeidLabel = new FormLabel();
        nodeidLabel.setText("Node ID");
        nodeidInput = new Input();
        nodeidInput.setType(InputType.NUMBER);
        nodeidInput.setPlaceholder("e.g. 2");
        nodeidGroup.add(nodeidLabel);
        nodeidGroup.add(nodeidInput);
        boxBody.add(nodeidGroup);
        //ROW 5

        //ROW 6
        FormGroup valuetypeGroup = new FormGroup();
        valuetypeGroup.setWidth(FORMWIDTH);
        valuetypeLabel = new FormLabel();
        valuetypeLabel.setText("Select a value type");
        valueTypeBox = new ListBox();
        valueTypeBox.addItem("---");
        valueTypeBox.addItem("integer");
        valueTypeBox.addItem("double");
        valuetypeGroup.add(valuetypeLabel);
        valuetypeGroup.add(valueTypeBox);
        boxBody.add(valuetypeGroup);
        //ROW 7
        FormGroup deltaGroup = new FormGroup();
        deltaGroup.setWidth(FORMWIDTH);
        deltaLabel = new FormLabel();
        deltaLabel.setText("Deviation value");
        deltaInput = new Input();
        deltaInput.setType(InputType.NUMBER);
        deltaInput.setPlaceholder("e.g. 0.25");
        deltaGroup.add(deltaLabel);
        deltaGroup.add(deltaInput);
        boxBody.add(deltaGroup);
        //ROW 8
        FormGroup unitGroup = new FormGroup();
        unitGroup.setWidth(FORMWIDTH);
        unitLabel = new FormLabel();
        unitLabel.setText("Unit");
        unitInput = new Input();
        unitInput.setType(InputType.TEXT);
        unitInput.setPlaceholder("e.g. °C");
        unitGroup.add(unitLabel);
        unitGroup.add(unitInput);
        boxBody.add(unitGroup);
        //adding body to panel
        boxPanel.add(boxBody);
    }

    private void loadFooter() {
        PanelFooter boxFooter = new PanelFooter();
        addButton = new Button("Add");
        addButton.setType(ButtonType.SUCCESS);
        cancelButton = new Button("Cancel");
        cancelButton.setPull(Pull.RIGHT);
        boxFooter.add(addButton);
        boxFooter.add(cancelButton);
        //adding footer to panel
        boxPanel.add(boxFooter);
    }

    public void loadValues() {
        machines.clear();
        machineListBox.clear();
        machineListBox.addItem("Loading...");
        machineListBox.setEnabled(false);
        sensorService.getActiveMachines(new AsyncCallback<Map<Integer, String>>() {
            @Override
            public void onFailure(Throwable caught) {

            }

            @Override
            public void onSuccess(Map<Integer, String> result) {
                machineListBox.clear();
                machineListBox.addItem("---");
                Set<Integer> keySet = result.keySet();
                Iterator<Integer> keyIt = keySet.iterator();
                while (keyIt.hasNext()) {
                    int curMid = keyIt.next();
                    machines.put(result.get(curMid),curMid);
                    machineListBox.addItem(result.get(curMid));
                }
                machineListBox.setEnabled(true);
            }
        });
    }

    public Map<String,String> getSensorData() {
        Map<String,String> sensor = new HashMap<>();

        if(sensornameInput.getValue().isEmpty()) {
            Notify.notify(ErrorsEN.ERROR_FIELD_REQUIRED_1+sensorNameLabel.getTitle()+ErrorsEN.ERROR_FIELD_REQUIRED_2, NotifyConfig.notifyError());
            return null;
        }
        if(machineListBox.getSelectedValue().equals("---")) {
            Notify.notify(ErrorsEN.ERROR_FIELD_REQUIRED_1+machineListLabel.getTitle()+ErrorsEN.ERROR_FIELD_REQUIRED_2, NotifyConfig.notifyError());
            return null;
        }
        if(namespaceInput.getValue().isEmpty()) {
            Notify.notify(ErrorsEN.ERROR_FIELD_REQUIRED_1+nameSpaceLabel.getTitle()+ErrorsEN.ERROR_FIELD_REQUIRED_2, NotifyConfig.notifyError());
            return null;
        }
        if(nodeidInput.getValue().isEmpty()) {
            Notify.notify(ErrorsEN.ERROR_FIELD_REQUIRED_1+nodeidLabel.getTitle()+ErrorsEN.ERROR_FIELD_REQUIRED_2, NotifyConfig.notifyError());
            return null;
        }
        if(valueTypeBox.getSelectedValue().equals("---")) {
            Notify.notify(ErrorsEN.ERROR_FIELD_REQUIRED_1+valuetypeLabel.getTitle()+ErrorsEN.ERROR_FIELD_REQUIRED_2, NotifyConfig.notifyError());
            return null;
        }
        if(deltaInput.getValue().isEmpty()) {
            Notify.notify(ErrorsEN.ERROR_FIELD_REQUIRED_1+deltaLabel.getTitle()+ErrorsEN.ERROR_FIELD_REQUIRED_2, NotifyConfig.notifyError());
            return null;
        }
        if(unitInput.getValue().isEmpty()) {
            Notify.notify(ErrorsEN.ERROR_FIELD_REQUIRED_1+unitLabel.getTitle()+ErrorsEN.ERROR_FIELD_REQUIRED_2, NotifyConfig.notifyError());
            return null;
        }

        sensor.put("sensorName", sensornameInput.getValue());
        sensor.put("machineID", machines.get(machineListBox.getSelectedValue()).toString());
        sensor.put("machineName", machineListBox.getSelectedValue());
        sensor.put("namespace", namespaceInput.getValue());
        sensor.put("nodeid", nodeidInput.getValue());
        sensor.put("valueType", valueTypeBox.getSelectedValue());
        sensor.put("delta", deltaInput.getValue());
        sensor.put("unit", unitInput.getValue());

        return sensor;
    }

    //SETTER ClickHandler
    public void setAddButtonHandler(ClickHandler clickHandler) {
        addButton.addClickHandler(clickHandler);
    }

    public void setCancelButtonHandler(ClickHandler clickHandler) {
        cancelButton.addClickHandler(clickHandler);
    }

}
