package client.assets.dialogs;

import client.config.ConfigService;
import client.config.ConfigServiceAsync;
import client.config.ZposConfig;
import com.google.gwt.dom.client.BRElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.*;
import org.gwtbootstrap3.extras.toggleswitch.client.ui.ToggleSwitch;

import java.util.HashMap;
import java.util.Map;

/**
 *  _____
 * |_   _|
 *   | |  _ __   __ _  ___ _ __   ___ ___
 *   | | | '_ \ / _` |/ _ \ '_ \ / __/ _ \
 *  _| |_| | | | (_| |  __/ | | | (_|  __/
 * |_____|_| |_|\__, |\___|_| |_|\___\___|
 *               __/ |
 *              |___/
 * https://ingence.de
 *
 * Created by Cedric Hopf on 25.08.2017
 * © https://github.com/CedricHopf
 **/
public class SettingsBox extends DialogBox {

    //Local elements
    private Panel boxPanel;
    private Button saveButton;
    private Button cancelButton;
    //|--- UI value fields
    private Input serverNameInput;
    private Input serverPortInput;
    private Input intervalInput;
    private ToggleSwitch enableNotifySwitch;
    //CONMON servlets
    private ConfigServiceAsync configService;

    public SettingsBox() {
        super(false,false);
        this.setText("Settings");
        this.getElement().getStyle().setZIndex(ZposConfig.getNextZpos());
        configService = ConfigService.Instance.get();
        loadContent();
    }

    private void loadContent() {
        boxPanel = new Panel();
        loadBody();
        loadFooter();
        this.add(boxPanel);
    }

    private void loadBody() {
        PanelBody boxBody = new PanelBody();
        //|---Server Settings
        //|---ROW 1
        FormGroup serverNameGroup = new FormGroup();
        FormLabel serverNameLabel = new FormLabel();
        serverNameLabel.setText("Servername");
        serverNameInput = new Input();
        serverNameInput.setType(InputType.TEXT);
        serverNameInput.setPlaceholder("e.g. ingence.de");
        serverNameGroup.add(serverNameLabel);
        serverNameGroup.add(serverNameInput);
        boxBody.add(serverNameGroup);
        //|---ROW 2
        FormGroup serverPortGroup = new FormGroup();
        FormLabel serverPortLabel = new FormLabel();
        serverPortLabel.setText("Serverport");
        serverPortInput = new Input();
        serverPortInput.setType(InputType.NUMBER);
        serverPortInput.setPlaceholder("e.g. 1080");
        serverPortGroup.add(serverPortLabel);
        serverPortGroup.add(serverPortInput);
        boxBody.add(serverPortGroup);
        //|---Application Settings
        //|---ROW 3
        FormGroup intervalGroup = new FormGroup();
        FormLabel intervalLabel = new FormLabel();
        intervalLabel.setText("Update interval (in ms)");
        intervalInput = new Input();
        intervalInput.setType(InputType.NUMBER);
        intervalInput.setPlaceholder("e.g. 1000");
        intervalGroup.add(intervalLabel);
        intervalGroup.add(intervalInput);
        boxBody.add(intervalGroup);
        //|---ROW 4
        FormGroup enableNotifyGroup = new FormGroup();
        FormLabel enableNotfiyLabel = new FormLabel();
        enableNotfiyLabel.setText("Show notifications");
        enableNotifySwitch = new ToggleSwitch();
        enableNotifySwitch.setOnIcon(IconType.CHECK);
        enableNotifySwitch.setOffIcon(IconType.TIMES);
        enableNotifyGroup.add(enableNotfiyLabel);
        enableNotifyGroup.getElement().appendChild(DOM.createElement(BRElement.TAG));
        enableNotifyGroup.add(enableNotifySwitch);
        boxBody.add(enableNotifyGroup);
        //adding panelbody to panel
        boxPanel.add(boxBody);
    }

    private void loadFooter() {
        PanelFooter boxFooter = new PanelFooter();
        saveButton = new Button("Save");
        saveButton.setType(ButtonType.SUCCESS);
        cancelButton = new Button("Cancel");
        cancelButton.setPull(Pull.RIGHT);
        boxFooter.add(saveButton);
        boxFooter.add(cancelButton);
        boxPanel.add(boxFooter);
    }

    public void loadValues() {
        //Servername
        serverNameInput.setValue("loading...");
        configService.getConfig("servername", new AsyncCallback<String>() {
            public void onFailure(Throwable caught) {
                serverNameInput.setValue("");
            }

            public void onSuccess(String result) {
                serverNameInput.setValue(result);
            }
        });
        //Serverport
        serverPortInput.setValue("loading...");
        configService.getConfig("serverport", new AsyncCallback<String>() {
            public void onFailure(Throwable caught) {
                serverPortInput.setValue("");
            }

            public void onSuccess(String result) {
                serverPortInput.setValue(result);
            }
        });
        //Update interval
        intervalInput.setValue("loading...");
        configService.getConfig("interval", new AsyncCallback<String>() {
            @Override
            public void onFailure(Throwable caught) {
                intervalInput.setValue("");
            }

            @Override
            public void onSuccess(String result) {
                intervalInput.setValue(result);
            }
        });
        //Enable Notifications
        enableNotifySwitch.setEnabled(false);
        configService.getConfig("notifications", new AsyncCallback<String>() {
            public void onFailure(Throwable caught) {
                enableNotifySwitch.setEnabled(true);
            }

            public void onSuccess(String result) {
                enableNotifySwitch.setEnabled(true);
                if(result == "enabled") {
                    enableNotifySwitch.setValue(true);
                } else {
                    enableNotifySwitch.setValue(false);
                }
            }
        });
    }

    public void setSaveButtonHandler(ClickHandler clickHandler) {
        saveButton.addClickHandler(clickHandler);
    }

    public void setCancelButtonHandler(ClickHandler clickHandler) {
        cancelButton.addClickHandler(clickHandler);
    }

    public void setSaveButtonLoading() {
        saveButton.state().loading();
    }

    public void setSaveButtonNotLoading() {
        saveButton.state().reset();
    }

    //GETTER
    public String getServername() {
        return serverNameInput.getValue();
    }

    public String getServerport() {
        return serverPortInput.getValue();
    }

    public String getInterval() {
        return intervalInput.getValue();
    }

    public boolean getNotificationsSettings() {
        return enableNotifySwitch.getValue();
    }

}
