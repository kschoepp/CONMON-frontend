package client.assets.dialogs;

import client.config.ZposConfig;
import client.locales.ErrorsEN;
import client.locales.MessagesEN;
import client.remote.SensorService;
import client.remote.SensorServiceAsync;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.*;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyPlacement;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyType;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;
import org.gwtbootstrap3.extras.notify.client.ui.NotifySettings;

import java.util.HashMap;
import java.util.Map;

/**
 *  _____
 * |_   _|
 *   | |  _ __   __ _  ___ _ __   ___ ___
 *   | | | '_ \ / _` |/ _ \ '_ \ / __/ _ \
 *  _| |_| | | | (_| |  __/ | | | (_|  __/
 * |_____|_| |_|\__, |\___|_| |_|\___\___|
 *               __/ |
 *              |___/
 * https://ingence.de
 *
 * Created by Cedric Hopf on 08.09.2017
 * © https://github.com/CedricHopf
 **/
public class AddRuleBox extends DialogBox {

    //Global variables
    private Map<Integer,String> aggregates;
    //|---UI elements
    private Panel boxPanel;
    private PanelBody boxBody;
    private PanelFooter boxFooter;
    private FormGroup ruleBlock;
    private Button addButton;
    private Button plusButton;
    private Button cancelButton;
    //CONMON servlets
    private SensorServiceAsync sensorService;

    //Constructor
    public AddRuleBox() {
        super(false,false);
        this.setText("Add new rule");
        this.getElement().getStyle().setZIndex(ZposConfig.getNextZpos());
        this.aggregates = new HashMap<>();
        sensorService = SensorService.Instance.get();
        loadContent();
        loadHandlers();
    }

    private void loadContent() {
        boxPanel = new Panel();
        loadBody();
        loadFooter();
        this.add(boxPanel);
    }

    private void loadBody() {
        boxBody = new PanelBody();
        Heading ruleHeading = new Heading(HeadingSize.H4);
        ruleHeading.setText("Define a new rule");
        boxBody.add(ruleHeading);
        ruleBlock = new FormGroup();
        ruleBlock.setSize(FormGroupSize.LARGE);
        ruleBlock.add(newParaminput());
        ruleBlock.add(newOperatorList());
        boxBody.add(ruleBlock);
        plusButton = new Button();
        plusButton.setIcon(IconType.PLUS);
        plusButton.setType(ButtonType.SUCCESS);
        plusButton.setPull(Pull.RIGHT);
        boxBody.add(plusButton);
        boxPanel.add(boxBody);
    }

    private void loadFooter() {
        boxFooter = new PanelFooter();
        addButton = new Button("Add");
        addButton.setType(ButtonType.SUCCESS);
        cancelButton = new Button("Cancel");
        cancelButton.setPull(Pull.RIGHT);
        boxFooter.add(addButton);
        boxFooter.add(cancelButton);
        //adding footer to panel
        boxPanel.add(boxFooter);
    }

    private Input newParaminput() {
        Input output = new Input();
        output.setPlaceholder("e.g. R1, A1, s1, etc");
        output.setType(InputType.TEXT);
        return output;
    }

    private ListBox newOperatorList() {
        ListBox output = new ListBox();
        output.addItem("END");
        output.addItem("&&");
        output.addItem("||");
        output.addItem("<");
        output.addItem(">");
        output.addItem("==");
        output.addItem("!=");
        output.addItem("<=");
        output.addItem(">=");
        return output;
    }

    private void loadHandlers() {
        plusButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ruleBlock.add(newParaminput());
                ruleBlock.add(newOperatorList());
            }
        });
        addButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                sensorService.getNextRID(new AsyncCallback<Integer>() {
                    @Override
                    public void onFailure(Throwable caught) { }

                    @Override
                    public void onSuccess(Integer result) {
                        int rid = result;
                        int ruleEntries = ruleBlock.getWidgetCount();
                        int subrid = 1;
                        int i = 0;
                        String sensorUsed = "";
                        while (i < ruleEntries) {
                            Input curInput = (Input) ruleBlock.getWidget(i);
                            if(curInput.getValue().contains("s")) {
                                sensorUsed = sensorUsed + curInput.getValue();
                            }
                            i = i+2;
                        }
                        i = 0;
                        while(i < ruleEntries) {
                            Input paramWidget = (Input) ruleBlock.getWidget(i);
                            String param = paramWidget.getValue();
                            i++;
                            ListBox operatorWidget = (ListBox) ruleBlock.getWidget(i);
                            String operator = operatorWidget.getSelectedValue();
                            i++;
                            sensorService.addRule(rid, subrid, "Rule" + rid, param, operator, sensorUsed, new AsyncCallback<Integer>() {
                                @Override
                                public void onFailure(Throwable caught) {
                                    NotifySettings settings = NotifySettings.newSettings();
                                    settings.setType(NotifyType.DANGER);
                                    settings.setPlacement(NotifyPlacement.TOP_CENTER);
                                    Notify.notify(ErrorsEN.ERROR_RULE_NOT_ADDED, settings);
                                }
                                @Override
                                public void onSuccess(Integer result) {
                                    if(result == 200) {
                                        NotifySettings settings = NotifySettings.newSettings();
                                        settings.setType(NotifyType.SUCCESS);
                                        settings.setPlacement(NotifyPlacement.TOP_RIGHT);
                                        Notify.notify(MessagesEN.MESSAGE_RULE_ADDED, settings);
                                    } else {
                                        NotifySettings settings = NotifySettings.newSettings();
                                        settings.setType(NotifyType.DANGER);
                                        settings.setPlacement(NotifyPlacement.TOP_CENTER);
                                        Notify.notify(ErrorsEN.ERROR_RULE_NOT_ADDED, settings);
                                    }
                                }
                            });
                            subrid++;
                        }
                    }
                });
            }
        });
        cancelButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                AddRuleBox.this.hide();
            }
        });
    }

}
