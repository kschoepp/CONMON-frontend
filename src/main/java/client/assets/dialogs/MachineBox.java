package client.assets.dialogs;

import client.config.NotifyConfig;
import client.config.ZposConfig;
import client.locales.ErrorsEN;
import client.locales.MessagesEN;
import client.remote.SensorService;
import client.remote.SensorServiceAsync;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.HeadingSize;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.Pull;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyPlacement;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyType;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;
import org.gwtbootstrap3.extras.notify.client.ui.NotifySettings;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *  _____
 * |_   _|
 *   | |  _ __   __ _  ___ _ __   ___ ___
 *   | | | '_ \ / _` |/ _ \ '_ \ / __/ _ \
 *  _| |_| | | | (_| |  __/ | | | (_|  __/
 * |_____|_| |_|\__, |\___|_| |_|\___\___|
 *               __/ |
 *              |___/
 * https://ingence.de
 *
 * Created by Cedric Hopf on 08.09.2017
 * © https://github.com/CedricHopf
 **/
public class MachineBox extends DialogBox {

    //Local elements
    private Panel boxPanel;
    private Button addButton;
    private Button closeButton;
    //|---UI field elements
    private ListGroup machineList;
    //CONMON servlets
    private SensorServiceAsync sensorService;

    public MachineBox() {
        super(false,false);
        this.setText("Active machines");
        this.getElement().getStyle().setZIndex(ZposConfig.getNextZpos());
        sensorService = SensorService.Instance.get();
        loadContent();
    }

    private void loadContent() {
        boxPanel = new Panel();
        loadBody();
        loadFooter();
        this.add(boxPanel);
    }

    private void loadBody() {
        PanelBody boxBody = new PanelBody();
        Heading machineHeading = new Heading(HeadingSize.H4);
        machineHeading.setText("Active machines");
        machineList = new ListGroup();
        machineList.setWidth("500px");
        boxBody.add(machineHeading);
        boxBody.add(machineList);
        boxPanel.add(boxBody);
    }

    private void loadFooter() {
        PanelFooter boxFooter = new PanelFooter();
        addButton = new Button();
        addButton.setType(ButtonType.SUCCESS);
        addButton.setIcon(IconType.PLUS);
        closeButton = new Button();
        closeButton.setText("Close");
        closeButton.setPull(Pull.RIGHT);
        boxFooter.add(addButton);
        boxFooter.add(closeButton);
        boxPanel.add(boxFooter);
    }

    public void loadValues() {
        machineList.clear();
        ListGroupItem loadingItem = new ListGroupItem();
        loadingItem.setText("Loading...");
        machineList.add(loadingItem);
        sensorService.getActiveMachines(new AsyncCallback<Map<Integer, String>>() {
            @Override
            public void onFailure(Throwable caught) {
                //TODO throw notify
            }

            @Override
            public void onSuccess(Map<Integer, String> result) {
                machineList.clear();
                if(result.size() != 0) {
                    Set<Integer> keys = result.keySet();
                    Iterator<Integer> keysIt = keys.iterator();
                    while (keysIt.hasNext()) {
                        int curKey = keysIt.next();
                        ListGroupItem item = new ListGroupItem();
                        item.setText(result.get(curKey));
                        item.setId(Integer.toString(curKey));
//                        Button infoButton = new Button();
//                        infoButton.setIcon(IconType.INFO);
//                        infoButton.setPull(Pull.RIGHT);
//                        infoButton.setType(ButtonType.PRIMARY);
//                        infoButton.addClickHandler(new ClickHandler() {
//                            @Override
//                            public void onClick(ClickEvent event) {
//                                //TODO open sensorinfobox
//                            }
//                        });
                        Button deleteButton = new Button();
                        deleteButton.setIcon(IconType.TRASH);
                        deleteButton.setPull(Pull.RIGHT);
                        deleteButton.setType(ButtonType.DANGER);
                        deleteButton.setMarginLeft(10);
                        deleteButton.addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent event) {
                                sensorService.deleteMachine(Integer.parseInt(item.getId()), new AsyncCallback<Integer>() {
                                    @Override
                                    public void onFailure(Throwable caught) {
                                        //TODO
                                    }

                                    @Override
                                    public void onSuccess(Integer result) {
                                        if(result == 1) {
                                            NotifySettings settings = NotifySettings.newSettings();
                                            settings.setType(NotifyType.SUCCESS);
                                            settings.setPlacement(NotifyPlacement.TOP_RIGHT);
                                            Notify.notify(MessagesEN.MESSAGE_MACHINE_REMOVED, settings);
                                            machineList.remove(item);
                                            loadValues();
                                        } else if(result == -3) {
                                            Notify.notify(ErrorsEN.ERROR_MACHINE_NOT_REMOVED_ACTIVE_SENSORS, NotifyConfig.notifyError());
                                        } else {
                                            NotifySettings settings = NotifySettings.newSettings();
                                            settings.setType(NotifyType.DANGER);
                                            settings.setPlacement(NotifyPlacement.TOP_CENTER);
                                            Notify.notify(ErrorsEN.ERROR_MACHINE_NOT_REMOVED, settings);
                                        }
                                    }
                                });
                            }
                        });
                        item.add(deleteButton);
                        //item.add(infoButton);
                        machineList.add(item);
                    }
                } else {
                    ListGroupItem item = new ListGroupItem();
                    item.setText("No active machines found!");
                    machineList.add(item);
                }
            }
        });
    }

    public void setAddButtonHandler(ClickHandler clickHandler) {
        addButton.addClickHandler(clickHandler);
    }

    public void setCloseButtonHandler(ClickHandler clickHandler) {
        closeButton.addClickHandler(clickHandler);
    }

}
