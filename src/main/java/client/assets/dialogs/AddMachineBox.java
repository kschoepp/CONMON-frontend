package client.assets.dialogs;

import client.config.NotifyConfig;
import client.config.ZposConfig;
import client.locales.ErrorsEN;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.DialogBox;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.InputType;
import org.gwtbootstrap3.client.ui.constants.Pull;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;

import java.util.HashMap;
import java.util.Map;

/**
 *  _____
 * |_   _|
 *   | |  _ __   __ _  ___ _ __   ___ ___
 *   | | | '_ \ / _` |/ _ \ '_ \ / __/ _ \
 *  _| |_| | | | (_| |  __/ | | | (_|  __/
 * |_____|_| |_|\__, |\___|_| |_|\___\___|
 *               __/ |
 *              |___/
 * https://ingence.de
 *
 * Created by Cedric Hopf on 25.08.2017
 * © https://github.com/CedricHopf
 **/
public class AddMachineBox extends DialogBox {

    private static final String FORMWIDTH = "300px";
    //Global variables
    private Panel boxPanel;
    private Button addButton;
    private Button cancelButton;
    //|---UI field elements
    private FormLabel machineNameLabel;
    private FormLabel uriLabel;
    private FormLabel locationLabel;
    private FormLabel descriptionLabel;
    private Input machinenameInput;
    private Input uriInput;
    private Input locationInput;
    private Input descriptionInput;


    //Constructor
    public AddMachineBox() {
        super(false,false);
        this.setText("Add new machine");
        this.getElement().getStyle().setZIndex(ZposConfig.getNextZpos());
        loadContent();
    }

    private void loadContent() {
        boxPanel = new Panel();
        loadBody();
        loadFooter();
        this.add(boxPanel);
    }

    private void loadBody() {
        PanelBody boxBody = new PanelBody();
        FormGroup machineNameGroup = new FormGroup();
        machineNameGroup.setWidth(FORMWIDTH);
        machineNameLabel = new FormLabel();
        machineNameLabel.setText("Machinename");
        machinenameInput = new Input();
        machinenameInput.setPlaceholder("e.g. Demo machine");
        machinenameInput.setType(InputType.TEXT);
        machineNameGroup.add(machineNameLabel);
        machineNameGroup.add(machinenameInput);
        boxBody.add(machineNameGroup);
        FormGroup uriGroup = new FormGroup();
        uriGroup.setWidth(FORMWIDTH);
        uriLabel = new FormLabel();
        uriLabel.setText("URI (port included)");
        uriInput = new Input();
        uriInput.setPlaceholder("e.g. localhost:1080");
        uriInput.setType(InputType.TEXT);
        uriGroup.add(uriLabel);
        uriGroup.add(uriInput);
        boxBody.add(uriGroup);
        FormGroup locGroup = new FormGroup();
        locGroup.setWidth(FORMWIDTH);
        locationLabel = new FormLabel();
        locationLabel.setText("Location");
        locationInput = new Input();
        locationInput.setPlaceholder("e.g. Office 123");
        locationInput.setType(InputType.TEXT);
        locGroup.add(locationLabel);
        locGroup.add(locationInput);
        boxBody.add(locGroup);
        FormGroup descGroup = new FormGroup();
        descGroup.setWidth(FORMWIDTH);
        descriptionLabel = new FormLabel();
        descriptionLabel.setText("Description");
        descriptionInput = new Input();
        descriptionInput.setPlaceholder("e.g. Does something");
        descriptionInput.setType(InputType.TEXT);
        descGroup.add(descriptionLabel);
        descGroup.add(descriptionInput);
        boxBody.add(descGroup);
        boxPanel.add(boxBody);
    }

    private void loadFooter() {
        PanelFooter boxFooter = new PanelFooter();
        addButton = new Button("Add");
        addButton.setType(ButtonType.SUCCESS);
        cancelButton = new Button("Cancel");
        cancelButton.setPull(Pull.RIGHT);
        boxFooter.add(addButton);
        boxFooter.add(cancelButton);
        //adding footer to panel
        boxPanel.add(boxFooter);
    }

    public Map<String,String> getMachineData() {
        Map<String,String> machine = new HashMap<>();

        if(machinenameInput.getValue().isEmpty() ) {//|| portInput.getValue().isEmpty()) {
            return null;
        }

        machine.put("machineName", machinenameInput.getValue());
        machine.put("uri",uriInput.getValue());
        machine.put("location", locationInput.getValue());
        machine.put("description", descriptionInput.getValue());

        return machine;
    }

    //SETTER ClickHandler
    public void setAddButtonHandler(ClickHandler clickHandler) {
        addButton.addClickHandler(clickHandler);
    }

    public void setCancelButtonHandler(ClickHandler clickHandler) {
        cancelButton.addClickHandler(clickHandler);
    }

}
