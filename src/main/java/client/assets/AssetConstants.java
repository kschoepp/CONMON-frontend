package client.assets;

/**
 *  _____
 * |_   _|
 *   | |  _ __   __ _  ___ _ __   ___ ___
 *   | | | '_ \ / _` |/ _ \ '_ \ / __/ _ \
 *  _| |_| | | | (_| |  __/ | | | (_|  __/
 * |_____|_| |_|\__, |\___|_| |_|\___\___|
 *               __/ |
 *              |___/
 * https://ingence.de
 *
 * Created by Cedric Hopf on 25.08.2017
 * © https://github.com/CedricHopf
 **/
public class AssetConstants {

    //Chartselect-Constants
    public static final int SELECT_GAUGECHART = 1;
    public static final int SELECT_LINECHART = 2;
    public static final int SELECT_BARCHART = 3;
    public static final int SELECT_AREACHART = 4;
    public static final int SELECT_NOTHING = -1;

    //Chartvalue-Constants
    public static final String VTYPE_INTEGER = "int";
    public static final String VTYPE_DOUBLE = "double";

}
