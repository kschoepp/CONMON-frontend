package client.remote;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


@RemoteServiceRelativePath("SensorService")
public interface SensorService extends RemoteService {
    /**
     * Utility/Convenience class.
     * Use SensorService.App.getInstance() to access static instance of SensorServiceAsync
     */
    class Instance {
        private static final SensorServiceAsync ourInstance = (SensorServiceAsync) GWT.create(SensorService.class);

        public static SensorServiceAsync get() {
            return ourInstance;
        }
    }

    Map<Integer,String> getActiveSensors();

    Map<Integer,String> getActiveSensorsByMachine(int mid);

    Map<Integer,String> getActiveMachines();

    Map<Integer,String> getActiveRules();

    Map<Integer,String> getActiveAggregates();

    Map<String,String> getSensorInfo(int sid);

    int getNextRID();

    int getNextAID();

    int addSensor(String sensorName, String machineName, String mid, String namespace, String nodeid, String valueType, String delta, String unit);

    int addMachine(String machineName, String uri, String location, String description);

    int addRule(int rid, int subrid, String rname, String parameter, String operator, String sensorused);

    int addAggregate(int aid, int subaid, String aname, String parameter, String operator, String sensorused);

    int addEvent(int rid, String type, String message, boolean trigger);

    int deleteSensor(int sid);

    int deleteMachine(int mid);

    int deleteRule(int rid);

    int deleteAggregate(int aid);

}
