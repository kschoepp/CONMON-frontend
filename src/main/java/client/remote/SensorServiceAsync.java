package client.remote;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.Map;


public interface SensorServiceAsync {

    void getActiveSensors(AsyncCallback<Map<Integer,String>> callback);

    void getActiveSensorsByMachine(int mid, AsyncCallback<Map<Integer,String>> callback);

    void getActiveMachines(AsyncCallback<Map<Integer,String>> callback);

    void getActiveRules(AsyncCallback<Map<Integer,String>> callback);

    void getActiveAggregates(AsyncCallback<Map<Integer,String>> callback);

    void getSensorInfo(int sid, AsyncCallback<Map<String,String>> callback);

    void getNextRID(AsyncCallback<Integer> callback);

    void getNextAID(AsyncCallback<Integer> callback);

    void addSensor(String sensorName, String machineName, String mid, String namespace, String nodeid, String valueType, String delta, String unit, AsyncCallback<Integer> callback);

    void addMachine(String machineName, String uri, String location, String description, AsyncCallback<Integer> callback);

    void addRule(int rid, int subrid, String rname, String parameter, String operator, String sensorused, AsyncCallback<Integer> callback);

    void addAggregate(int aid, int subaid, String aname, String parameter, String operator, String sensorused, AsyncCallback<Integer> callback);

    void addEvent(int rid, String type, String message, boolean trigger, AsyncCallback<Integer> callback);

    void deleteSensor(int sid, AsyncCallback<Integer> callback);

    void deleteMachine(int mid, AsyncCallback<Integer> callback);

    void deleteRule(int rid, AsyncCallback<Integer> callback);

    void deleteAggregate(int aid, AsyncCallback<Integer> callback);

}
