package server.config;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import client.config.ConfigService;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.*;


public class ConfigServiceImpl extends RemoteServiceServlet implements ConfigService {

    private Document doc;

    public ConfigServiceImpl() {
        doc = new Document();
        Element rootElement = new Element("config");
        Element rowElement = new Element("rows");
        Element widgetsElement = new Element("widgets");
        rootElement.addContent(rowElement);
        rootElement.addContent(widgetsElement);
        doc.setRootElement(rootElement);
    }

    public String getConfig(String configParam) {
        try {
            Properties p = new Properties();
            BufferedInputStream b = new BufferedInputStream(new FileInputStream("config/application.properties"));
            p.load(b);
            b.close();
            return p.getProperty(configParam);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    @Override
    public int writeConfigs(String serverName, String serverPort, String interval, String notificationsEnabled) {
        try {
            Properties p = new Properties();
            BufferedInputStream b = new BufferedInputStream(new FileInputStream("config/application.properties"));
            p.load(b);
            b.close();
            p.setProperty("servername",serverName);
            p.setProperty("serverport",serverPort);
            p.setProperty("notifications",notificationsEnabled);
            p.setProperty("interval", interval);
            File file = new File("config/application.properties");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            p.store(fileOutputStream,"Application Settings");
            return 1;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    @Override
    public int saveWidget(String widgetType, String rowID, String chartID, String sid, double min, double max) {
        try {
            Element newWidget = new Element("chart");
            Element eType = new Element("type");
            eType.setText(widgetType);
            Element eRID = new Element("rowID");
            eRID.setText(rowID);
            Element eCID = new Element("chartID");
            eCID.setText(chartID);
            Element eSID = new Element("sensorID");
            eSID.setText(sid);
            Element chartMin = new Element("min");
            chartMin.setText(Double.toString(min));
            Element chartMax = new Element("max");
            chartMax.setText(Double.toString(max));
            newWidget.addContent(eType);
            newWidget.addContent(eRID);
            newWidget.addContent(eCID);
            newWidget.addContent(eSID);
            newWidget.addContent(chartMin);
            newWidget.addContent(chartMax);
            doc.getRootElement().getChild("widgets").addContent(newWidget);
            //Writing XML data to file
            Format format = Format.getPrettyFormat();
            format.setIndent("   ");
            FileOutputStream fileOutputStream = new FileOutputStream("config/dashboard.xml");
            XMLOutputter xmlOutputter = new XMLOutputter(format);
            xmlOutputter.output(doc,fileOutputStream);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    @Override
    public int saveRow(String rowID, String rowName) {
        try {
            Element newRow = new Element("row");
            Element newRowID = new Element("rowID");
            newRowID.setText(rowID);
            Element newRowName = new Element("rowName");
            newRowName.setText(rowName);
            newRow.addContent(newRowID);
            newRow.addContent(newRowName);
            doc.getRootElement().getChild("rows").addContent(newRow);
            //Writing XML data to file
            Format format = Format.getPrettyFormat();
            format.setIndent("   ");
            FileOutputStream fileOutputStream = new FileOutputStream("config/dashboard.xml");
            XMLOutputter xmlOutputter = new XMLOutputter(format);
            xmlOutputter.output(doc,fileOutputStream);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    @Override
    public Map<String, String> getRows() {
        Map<String,String> rows = new HashMap<>();
        try {
            File xmlFile = new File("config/dashboard.xml");
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(xmlFile);
            Element rootElement = document.getRootElement();
            List<Element> rowElements = rootElement.getChild("rows").getChildren();
            for(int i=0;i<rowElements.size();i++) {
                Element curRow = rowElements.get(i);
                rows.put(curRow.getChildText("rowID"),curRow.getChildText("rowName"));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return rows;
    }

    @Override
    public ArrayList<Map<String,String>> getWidgets() {
        ArrayList<Map<String,String>> widgets = new ArrayList<>();
        try {
            File xmlFile = new File("config/dashboard.xml");
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(xmlFile);
            Element rootElement = document.getRootElement();
            List<Element> widgetElements = rootElement.getChild("widgets").getChildren();
            for(int i=0;i<widgetElements.size();i++) {
                Element curWidget = widgetElements.get(i);
                Map<String,String> cWidgetMap = new HashMap<>();
                cWidgetMap.put("type", curWidget.getChildText("type"));
                cWidgetMap.put("rowID", curWidget.getChildText("rowID"));
                cWidgetMap.put("chartID", curWidget.getChildText("chartID"));
                cWidgetMap.put("sensorID", curWidget.getChildText("sensorID"));
                cWidgetMap.put("min", curWidget.getChildText("min"));
                cWidgetMap.put("max", curWidget.getChildText("max"));
                widgets.add(cWidgetMap);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return widgets;
    }
}