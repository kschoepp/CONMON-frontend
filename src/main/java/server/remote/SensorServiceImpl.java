package server.remote;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import client.remote.SensorService;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import org.json.JSONArray;
import org.json.JSONObject;
import server.config.ConfigServiceImpl;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


public class SensorServiceImpl extends RemoteServiceServlet implements SensorService {

    //Global variables
    private String protocol;
    private String port;
    private String hostname;
    //CONMON servlets
    private ConfigServiceImpl configService;

    public SensorServiceImpl() {
        //loading serverconfigs from ConfigService
        configService = new ConfigServiceImpl();
        protocol = configService.getConfig("protocol");
        hostname = configService.getConfig("servername");
        port = configService.getConfig("serverport");
    }

    public Map<Integer, String> getActiveSensors() {
        Map<Integer, String> activeSensors = new HashMap<>();

        String uri = "/api/sensors";
        //GET method to server
        try {
            HttpResponse<JsonNode> response = Unirest.get(protocol + "://" + hostname + ":" + port + uri).asJson();
            JsonNode jsonNode = response.getBody();
            JSONArray jsonArray = jsonNode.getArray();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject curJson = jsonArray.getJSONObject(i);
                int sid = curJson.getInt("sensorID");
                String sname = curJson.getString("sensorName");
                activeSensors.put(sid, sname);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return activeSensors;
    }

    public Map<Integer, String> getActiveSensorsByMachine(int mid) {
        Map<Integer, String> sensors = new HashMap<>();

        String uri = "/api/sensors/bymachine/";

        //GET method to server
        try {
            HttpResponse<JsonNode> response = Unirest.get(protocol + "://" + hostname + ":" + port + uri + mid).asJson();
            JsonNode jsonNode = response.getBody();
            JSONArray jsonArray = jsonNode.getArray();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject curJson = jsonArray.getJSONObject(i);
                int sid = curJson.getInt("sensorID");
                String sname = curJson.getString("sensorName");
                sensors.put(sid, sname);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sensors;
    }

    public Map<Integer, String> getActiveMachines() {
        Map<Integer, String> activeMachines = new HashMap<>();

        String uri = "/api/machines";
        //GET method to server
        try {
            HttpResponse<JsonNode> response = Unirest.get(protocol + "://" + hostname + ":" + port + uri).asJson();
            JsonNode jsonNode = response.getBody();
            JSONArray jsonArray = jsonNode.getArray();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject curJson = jsonArray.getJSONObject(i);
                int mid = curJson.getInt("machineID");
                String mname = curJson.getString("machineName");
                activeMachines.put(mid, mname);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return activeMachines;
    }

    public Map<String, String> getSensorInfo(int sid) {
        Map<String, String> output = new HashMap<>();

        String uri = "/api/sensors/";
        //GET method to server
        try {
            HttpResponse<JsonNode> response = Unirest.get(protocol + "://" + hostname + ":" + port + uri + sid).asJson();
            JsonNode jsonNode = response.getBody();
            JSONObject jsonObject = jsonNode.getObject();
            String sensorName = jsonObject.getString("sensorName");
            int machineId = jsonObject.getInt("machineID");
            String machineName = jsonObject.getString("machineName");
            int namespace = jsonObject.getInt("namespace");
            int nodeid = jsonObject.getInt("nodeid");
            String createTS = jsonObject.getString("createTS");
            String value = jsonObject.getString("value");
            String valueType = jsonObject.getString("valueType");
            double delta = jsonObject.getDouble("deltaValue");
            String unit = jsonObject.getString("unit");
            String timeStamp = jsonObject.getString("timeStamp");
            String min = jsonObject.getString("min");
            String avg = jsonObject.getString("avg");
            String max = jsonObject.getString("max");

            output.put("sensorID", Integer.toString(sid));
            output.put("sensorName", sensorName);
            output.put("machineID", Integer.toString(machineId));
            output.put("machineName", machineName);
            output.put("namespace", Integer.toString(namespace));
            output.put("nodeid", Integer.toString(nodeid));
            output.put("createTS", createTS);
            output.put("value", value);
            output.put("valueType", valueType);
            output.put("deltaValue", Double.toString(delta));
            output.put("unit", unit);
            output.put("timeStamp", timeStamp);
            output.put("min", min);
            output.put("avg", avg);
            output.put("max", max);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return output;
    }

    public int addSensor(String sensorName, String machineName, String mid, String namespace, String nodeid, String valueType, String delta, String unit) {
        String uri = "/api/sensors/";
        //POST method to server
        try {
            HttpResponse<String> response = Unirest.post(protocol + "://" + hostname + ":" + port + uri)
                    .queryString("sensorName",sensorName)
                    .queryString("machineID", mid)
                    .queryString("machineName", machineName)
                    .queryString("namespace", namespace)
                    .queryString("nodeid", nodeid)
                    .queryString("valueType", valueType)
                    .queryString("delta", delta)
                    .queryString("unit", unit)
                    .asString();
            if(response.getStatus() == 200) {
                return 1;
            } else {
                return -1;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    public int addMachine(String machineName, String uri, String location, String description) {
        String url = "/api/machines";
        //POST methode to server
        try {
            HttpResponse<String> response = Unirest.post(protocol + "://" + hostname + ":" + port + url)
                    .queryString("machineName", URLEncoder.encode(machineName,"UTF-8"))
                    .queryString("uri", URLEncoder.encode(uri,"UTF-8"))
                    .queryString("location", URLEncoder.encode(location,"UTF-8"))
                    .queryString("description", URLEncoder.encode(description,"UTF-8"))
                    .asString();
            if (response.getStatus() == 200) {
                return 1;
            } else {
                return -1;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    public int deleteSensor(int sid) {
        String uri = "/api/sensors/";
        //DELETE method to server
        try {
            HttpResponse<String> response = Unirest.delete(protocol + "://" + hostname + ":" + port + uri + sid).asString();
            String result = response.getBody();
            if (result.equals("failed")) {
                return -1;
            } else {
                return 1;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    public int deleteMachine(int mid) {
        String uri = "/api/machines/";
        //DELETE method to server
        try {
            HttpResponse<String> response = Unirest.delete(protocol+"://"+hostname+":"+port+uri+mid).asString();
            String result = response.getBody();
            if(result.equals("success")) {
                return 1;
            } else if(result.equals("errorSensorsActive")) {
                return -3;
            } else {
                return -1;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    @Override
    public Map<Integer, String> getActiveRules() {
        Map<Integer,String> rules = new HashMap<>();

        String uri = "/api/rules";
        //GET method to server
        try {
            HttpResponse<JsonNode> response = Unirest.get(protocol + "://" + hostname + ":" + port + uri).asJson();
            JsonNode jsonNode = response.getBody();
            JSONArray jsonArray = jsonNode.getArray();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject curJson = jsonArray.getJSONObject(i);
                int ruleID = curJson.getInt("ruleID");
                String ruleName = curJson.getString("ruleName");
                rules.put(ruleID,ruleName);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return rules;
    }

    @Override
    public Map<Integer, String> getActiveAggregates() {
        Map<Integer,String> rules = new HashMap<>();

        String uri = "/api/aggregates";
        //GET method to server
        try {
            HttpResponse<JsonNode> response = Unirest.get(protocol + "://" + hostname + ":" + port + uri).asJson();
            JsonNode jsonNode = response.getBody();
            JSONArray jsonArray = jsonNode.getArray();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject curJson = jsonArray.getJSONObject(i);
                int aggregateID = curJson.getInt("aggregateID");
                String aggregateName = curJson.getString("aggregateName");
                rules.put(aggregateID,aggregateName);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return rules;
    }

    @Override
    public int addRule(int rid, int subrid, String rname, String parameter, String operator, String sensorused) {
        String uri = "/api/rules";
        //POST method to server
        try {
            HttpResponse<String> response = Unirest.post(protocol + "://" + hostname + ":" + port + uri)
                    .queryString("rid", rid)
                    .queryString("subrid", subrid)
                    .queryString("ruleName", rname)
                    .queryString("param",parameter)
                    .queryString("operator", operator)
                    .queryString("sensorused", URLEncoder.encode(sensorused,"UTF-8"))
                    .asString();
            return response.getStatus();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    @Override
    public int addAggregate(int aid, int subaid, String aname, String parameter, String operator, String sensorused) {
        String uri = "/api/aggregate";
        //POST method to server
        try {
            HttpResponse<String> response = Unirest.post(protocol + "://" + hostname + ":" + port + uri)
                    .queryString("aid", aid)
                    .queryString("subaid", subaid)
                    .queryString("aname", aname)
                    .queryString("parameter", parameter)
                    .queryString("operator", operator)
                    .queryString("sensorused", URLEncoder.encode(sensorused,"UTF-8"))
                    .asString();
            return response.getStatus();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    @Override
    public int addEvent(int rid, String type, String message, boolean trigger) {
        String uri = "/api/event";
        //POST method to server
        try {
            HttpResponse<String> response = Unirest.post(protocol + "://" + hostname + ":" + port + uri)
                    .queryString("rid", rid)
                    .queryString("eventType", type)
                    .queryString("message", URLEncoder.encode(message,"UTF-8"))
                    .queryString("triggerValue", trigger)
                    .asString();
            return response.getStatus();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    @Override
    public int getNextRID() {
        String uri = "/api/rules/id";
        //GET method to server
        try {
            HttpResponse<String> response = Unirest.get(protocol + "://" + hostname + ":" + port + uri)
                    .asString();
            System.out.println(response.getBody());
            return Integer.parseInt(response.getBody());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    @Override
    public int getNextAID() {
        String uri = "/api/aggregates/id";
        //GET method to server
        try {
            HttpResponse<String> response = Unirest.get(protocol + "://" + hostname + ":" + port + uri)
                    .asString();
            return Integer.parseInt(response.getBody());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    @Override
    public int deleteRule(int rid) {
        String uri = "/api/rule/";
        //DELETE method to server
        try {
            HttpResponse<String> response = Unirest.delete(protocol+"://"+hostname+":"+port+uri+rid).asString();
            if(response.getStatus() == 200) {
                return 1;
            } else {
                return -1;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }

    @Override
    public int deleteAggregate(int aid) {
        String uri = "/api/aggregate/";
        //DELETE method to server
        try {
            HttpResponse<String> response = Unirest.delete(protocol+"://"+hostname+":"+port+uri+aid).asString();
            if(response.getStatus() == 200) {
                return 1;
            } else {
                return -1;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }
}